#include "debugwindow.h"
#include "ui_debugwindow.h"

#include <QtGui>
#include <QWidget>


//player colors
QColor PlayerColor[7] = {Qt::darkGray, Qt::darkCyan, Qt::red, Qt::blue, Qt::green, Qt::cyan, Qt::magenta};

DebugWindow::DebugWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DebugWindow),
    m_inputHelper(nullptr)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(repaint()));
    timer->start(50);
}

QPoint transformPoint(const CGLA::Vec3f& point)
{
    QPoint result;
    result.setX(200 + 200 * point[0]);
    result.setY(200 - 200 * point[1]);
    return result;
}

void paintLimb(YA3Joint fromJoint, YA3Joint toJoint, QPainter& painter, const InputHelper& input, bool rawData)
{
    if(rawData)
        painter.drawLine(transformPoint(input.RawJointPosition(fromJoint)), transformPoint(input.RawJointPosition(toJoint)));
    else
        painter.drawLine(transformPoint(input.JointPosition(fromJoint)), transformPoint(input.JointPosition(toJoint)));
}

void paintSkeleton(QPainter& painter, const QPen& linePen, const QPen& jointPen, const InputHelper& input, bool rawData)
{
    painter.setPen(linePen);

    //Left arm
    paintLimb(YA3Joint_LHand, YA3Joint_LElbow, painter, input, rawData);
    paintLimb(YA3Joint_LElbow, YA3Joint_LShoulder, painter, input, rawData);

    //Right arm
    paintLimb(YA3Joint_RHand, YA3Joint_RElbow, painter, input, rawData);
    paintLimb(YA3Joint_RElbow, YA3Joint_RShoulder, painter, input, rawData);

    //Torso
    paintLimb(YA3Joint_LShoulder, YA3Joint_ShoulderCenter, painter, input, rawData);
    paintLimb(YA3Joint_RShoulder, YA3Joint_ShoulderCenter, painter, input, rawData);
    paintLimb(YA3Joint_LShoulder, YA3Joint_HipCenter, painter, input, rawData);
    paintLimb(YA3Joint_RShoulder, YA3Joint_HipCenter, painter, input, rawData);
    paintLimb(YA3Joint_ShoulderCenter, YA3Joint_HipCenter, painter, input, rawData);

    //Joint points
    painter.setPen(jointPen);
    for(int i = 0; i < YA3_JOINT_COUNT; ++i)
    {
        const CGLA::Vec3f& p = rawData ? input.RawJointPosition((YA3Joint)i) : input.JointPosition((YA3Joint)i);
        painter.drawEllipse(transformPoint(p), 4, 4);
    }
}

void DebugWindow::paintEvent(QPaintEvent*)
{
    if(m_inputHelper == nullptr)
        return;

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPen gridPen(Qt::gray, 1, Qt::DotLine);
    painter.setPen(gridPen);

    int w = 40;
    int n = 15;
    for(int i = 0; i < n; ++i)
    {
        painter.drawLine(i * w, 0, i * w, 800);
        painter.drawLine(0, i * w, 800, i * w);

    }

    QPen smoothLimbPen (PlayerColor[m_inputHelper->m_currentPlayer+1], 2, Qt::SolidLine);
    QPen smoothJointPen(PlayerColor[m_inputHelper->m_currentPlayer+1], 3, Qt::SolidLine);

    QPen rawLimbPen (Qt::gray, 1, Qt::DashLine);
    QPen rawJointPen(Qt::gray, 2, Qt::DashLine);

    paintSkeleton(painter, rawLimbPen, rawJointPen, *m_inputHelper, true);
    paintSkeleton(painter, smoothLimbPen, smoothJointPen, *m_inputHelper, false);
}


DebugWindow::~DebugWindow()
{
    delete ui;
}
