#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <string>
#include <vector>
#include <CGLA/Vec4f.h>
#include "Texmap.h"
#include <GLGraphics/ShaderProgram.h>

namespace Mesh
{
    /// A simple struct containing material properties.
	struct Material
	{
		std::string name;			   
        CGLA::Vec4f diffuse;
		CGLA::Vec4f specular;
        float shininess;
        GLuint mtl_list;
        Texmap tex_map;
        bool has_texture;
        CGLA::Vec4f ambient;
        CGLA::Vec4f transmission;
        float illum;
        float ior;
        


        Material()
            :name("default"),
              diffuse(0.8,0.8,0.8,1.0),
              specular(0.0,0.0,0.0,1.0),
              shininess(0),
              mtl_list(0),
              has_texture(false),
              ambient(0.0,0.0,0.0,1.0),
              transmission(0.0,0.0,0.0,1.0),
              illum(0),
              ior(0)
		{
        }
	};
}

#endif
