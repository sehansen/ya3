#ifndef SOUNDSYSTEM_H
#define SOUNDSYSTEM_H

#include <QObject>
#include <QSoundEffect>
#include <QCoreApplication>

class SoundSystem : public QObject
{
    Q_OBJECT

    QSoundEffect pulsesMusic;

public:
    explicit SoundSystem(QObject *parent = 0);

signals:

public slots:

};

#endif // SOUNDSYSTEM_H
