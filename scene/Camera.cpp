#include "Camera.h"
using namespace GLGraphics;
using namespace CGLA;
void Camera::setLightAndCamera(ShaderProgramDraw& shader_prog) const
{
    static const Vec4f lightSpecular(0.6f,0.6f,0.3f,0.6f);
    static const Vec4f lightDiffuse(0.6f,0.6f,0.3f,0.6f);
    static const Vec4f lightAmbient(0.3f,0.4f,0.6f,0.4f);

    shader_prog.set_projection_matrix(perspective_Mat4x4f(55, float(width)/height, 0.01f, 100));
    shader_prog.set_view_matrix(getViewMatrix());
    shader_prog.set_light_position(lightingPos);
    shader_prog.set_light_intensities(lightDiffuse, lightSpecular, lightAmbient);
}

const CGLA::Vec2f Camera::getRotations() const {
    float inclination = acos(dir[2]/sqrtf(powf(dir[0], 2)+powf(dir[1], 2)+powf(dir[2], 2)));
    float azimuth = atan2f(dir[1], dir[0]);
    return CGLA::Vec2f(inclination, azimuth);
}

void Camera::event(QEvent *event) {
    switch(event->type()) {
    case QEvent::KeyPress:
        keyPressEvent(static_cast<QKeyEvent*>(event));
        break;
    case QEvent::KeyRelease:
        keyReleaseEvent(static_cast<QKeyEvent *>(event));
        break;
    case QEvent::MouseButtonPress:
        mousePressEvent(static_cast<QMouseEvent *>(event));
        break;
    case QEvent::MouseButtonRelease:
        mouseReleaseEvent(static_cast<QMouseEvent *>(event));
        break;
    case QEvent::MouseMove:
        mouseMoveEvent(static_cast<QMouseEvent *>(event));
        break;
    }
 }
