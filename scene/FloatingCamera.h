#ifndef FLOATINGCAMERA_H
#define FLOATINGCAMERA_H
#include <Qt>
#include "Camera.h"
#include <GLGraphics/GLHeader.h>
#include <unordered_map>
#include <math.h>
#include <cmath>
class FloatingCamera : public Camera {
    CGLA::Vec3f targetPosition;
    float distance;
    float viewAngleX;
    float viewAngleY;
    float animationTime;
    float time;
public:
    FloatingCamera(const CGLA::Vec4f & lightingPos);
protected:
    virtual void mousePressEvent(QMouseEvent *) {}
    virtual void mouseReleaseEvent(QMouseEvent *) {}
    virtual void mouseMoveEvent(QMouseEvent *) {}
    virtual void keyPressEvent(QKeyEvent *) {}
    virtual void keyReleaseEvent(QKeyEvent *) {}
    virtual void update(float dt);
};
#endif // FLOATINGCAMERA_H
