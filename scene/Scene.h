#ifndef SCENE_H
#define SCENE_H

#include <algorithm>
#include <vector>

#include <Qt>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QTime>
#include <QTimer>
#include <GLGraphics/GLHeader.h>
#include <GLGraphics/ThreeDObject.h>
#include <GLGraphics/ShaderProgram.h>
#include <GLGraphics/User.h>

#include "../rendering/ToonRenderer.h"
#include "../interaction/inputhelper.h"
#include "../interaction/keyboardhelper.h"
#include "../interaction/kinecthelper.h"
#include "../rendering/ShaderCache.h"
#include "../scene/objects/Scene3DObject.h"
#include "../scene/objects/SceneMeshObject.h"
#include "../scene/objects/Terrain.h"
#include "../scene/objects/Tree.h"
#include "../rendering/Renderer.h"

#include "MovableCamera.h"
#include "FloatingCamera.h"

class Scene : public QGLWidget {
    Q_OBJECT

    QTime * time;
    QTimer * timer;
    float lastFrameTime;
    Renderer * renderer;
    Camera * camera;
    std::vector<SceneObject*> objects;
    std::vector<Tree*> trees;
    InputHelper * inputHelper;

public:
    Scene( const QGLFormat& format, QWidget* parent = 0);
    ~Scene();

    void setRenderer(Renderer *);
    InputHelper & getInputHelper();

    void setupScene();

    //QGlWidget overrides
    virtual void paintGL(); //Called each frame
    virtual void initializeGL(); //Init the gl context
    virtual void resizeGL(int width, int height ); //Called when the window is resized

    //Input
    virtual void Scene::mousePressEvent(QMouseEvent *m);
    virtual void Scene::mouseReleaseEvent(QMouseEvent *m);
    virtual void Scene::mouseMoveEvent(QMouseEvent *m);
    virtual void Scene::keyPressEvent(QKeyEvent *keyEvent);
    virtual void Scene::keyReleaseEvent(QKeyEvent *keyEvent);
    virtual bool event( QEvent * event );

public slots:
    void update();
    void updateInteraction(float);

};

#endif // SCENE_H
