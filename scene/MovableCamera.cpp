#include "MovableCamera.h"

using namespace CGLA;
using namespace GLGraphics;


const float MovableCamera::MOVE_SPEED = 2.0f;
const float MovableCamera::STRAFE_SPEED = 1.5f;
const float MovableCamera::LOOK_SPEED = 0.5f;

MovableCamera::MovableCamera(const Vec4f& lightingPos , QGLWidget & window):
    Camera(lightingPos),
    forward(Qt::Key_W), backward(Qt::Key_S),
    strafeLeft(Qt::Key_A), strafeRight(Qt::Key_D),
    lookButton(Qt::LeftButton), resetPosition(Qt::Key_O), isLooking(false), mousePressPos(Vec2i(0, 0)), window(window),
    pressedKeys(){
}

void MovableCamera::mousePressEvent(QMouseEvent * mouseEvent){
    Qt::MouseButton button = static_cast<Qt::MouseButton>(mouseEvent->button());
    if (button == lookButton) {
        isLooking = true;
        mousePressPos = CGLA::Vec2i(mouseEvent->x(), mouseEvent->y());

        window.setCursor(Qt::BlankCursor);
    }
}
void MovableCamera::mouseReleaseEvent(QMouseEvent * mouseEvent){
    Qt::MouseButton button = static_cast<Qt::MouseButton>(mouseEvent->button());
    if (button == lookButton) {
        isLooking = false;
        window.unsetCursor();
    }
}
void MovableCamera::mouseMoveEvent(QMouseEvent * mouseEvent){

    auto center = window.mapToGlobal(window.rect().center());
//    if (mouseEvent->x() == center.x() && mouseEvent->y() == center.y())
//        return;
//    mousePressPos[0] = center.x();
//    mousePressPos[1] = center.y();
    Vec2f rotation = getRotations(); //(inclination , azimuth)
    float delta_ang_x = M_PI*(mousePressPos[0]-mouseEvent->x())/getWidth();
    float delta_ang_y = -M_PI*(mousePressPos[1]-mouseEvent->y())/getHeight();
    float inclination = rotation[0] + delta_ang_y;
    float azimuth = rotation[1] + delta_ang_x;
//    std::cout << "mouse x: "<< mouseEvent->x() << " y: " << mouseEvent->y() <<
//                 " dx: "<< (mousePressPos[0]-mouseEvent->x()) << " dy: " <<
//                 (mousePressPos[1]-mouseEvent->y()) <<
//                 " inclination: "<< inclination << " azimuth: "<<azimuth<<
//                 " inclinationb: "<< rotation[0] << " azimuthb: "<<rotation[1]<<
//                 " dax: "<< delta_ang_x << " day: "<<delta_ang_y<<std::endl;

    setDirection(CGLA::Vec3f(sinf(inclination)*cosf(azimuth), sinf(inclination)*sinf (azimuth), cos(inclination) ));

    mousePressPos = CGLA::Vec2i(mouseEvent->x(), mouseEvent->y());
    //QCursor::setPos(center);
    /*float delta_ang_x = M_PI*(mousePressPos[0]-mouseEvent->x())/getWidth();
    float delta_ang_y = M_PI*(mousePressPos[1]-mouseEvent->y())/getHeight();
    float ax = ang_x + delta_ang_x;
    float ay = ang_y + delta_ang_y;
    Vec3f dir(cos(ax)*cos(ay),sin(ax)*cos(ay),sin(ay));
    user.set_dir(dir);*/
}
void MovableCamera::keyPressEvent(QKeyEvent * keyEvent){
    Qt::Key key = static_cast<Qt::Key>(keyEvent->key());
    pressedKeys[key] = true;
    if (key == resetPosition) {
        setPosition(Vec3f(-7.60765f, 14.907f, 4.37377f));
        setDirection(Vec3f(0.333226f, -0.925571f, 0.179661f));
    }
}
void MovableCamera::keyReleaseEvent(QKeyEvent * keyEvent){
    Qt::Key key = static_cast<Qt::Key>(keyEvent->key());
    pressedKeys[key] = false;
}

void MovableCamera::update(float deltaTime) {
    //std::cout << "delta time: "<< deltaTime << std::endl;
    if (checkIfKeyIsPressed(forward)) {
        movePosition(CGLA::Vec2f(MOVE_SPEED, 0)*deltaTime);
    } else if (checkIfKeyIsPressed(backward)) {
        movePosition(CGLA::Vec2f(-MOVE_SPEED, 0)*deltaTime);
    }
    if (checkIfKeyIsPressed(strafeLeft)) {
        movePosition(CGLA::Vec2f(0,STRAFE_SPEED)*deltaTime);
    } else if (checkIfKeyIsPressed(strafeRight)) {
        movePosition(CGLA::Vec2f(0,-STRAFE_SPEED)*deltaTime);
    }
}

