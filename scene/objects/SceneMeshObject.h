#ifndef SCENEMESHOBJECT_H
#define SCENEMESHOBJECT_H
#include <string>
#include <GLGraphics/ShaderProgram.h>
#include "../scene/SceneObject.h"
#include "../rendering/ShaderCache.h"
class SceneMeshObject : public SceneObject {
public:
    SceneMeshObject(ShaderCache::Shaders shader, CGLA::Vec3f pos=CGLA::Vec3f(0, 0, 0),
                    CGLA::Vec3f rot=CGLA::Vec3f(0, 0, 0), CGLA::Vec3f scale=CGLA::Vec3f(1, 1, 1)) :
        SceneObject(shader, pos, rot, scale){}
    virtual void draw(GLGraphics::ShaderProgramDraw&) {}

    virtual void update(float){}
};

#endif // SCENEMESHOBJECT_H
