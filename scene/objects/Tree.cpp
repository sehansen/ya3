#include "Tree.h"
#include "../flags.h"
using namespace CGLA;
using namespace GLGraphics;
using namespace std;
Tree::Tree() : SceneMeshObject(ShaderCache::GBufferRenderTree), spread(1.0f), tree(Vec3f(4,4,3)) {
    //createMaterial();
}

void Tree::createMaterial() {
    float brown = 0.25*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    float green = 0.15*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    float gray  = 0.25*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    mat_diff = Vec4f(0.2+gray+brown*2,
                     0.1+gray+brown+green,
                     gray,1);
}

//            M[i] = transpose(translation_Mat4x4f(p)*scaling_Mat4x4f(Vec3f(0.006 + scale))*rotation_Mat4x4f(ZAXIS, angle));
void Tree::createTransform() {
    transform = transpose(translation_Mat4x4f(getPosition())*scaling_Mat4x4f(getScale())*rotation_Mat4x4f(ZAXIS, getRotation()[1]));
}

void Tree::update(float timeStep) {
    tree.Update(timeStep);
}

GLuint createVAO(QQueue<Vec3f>& triangles, QQueue<Vec3f>& normals)
{
    GLuint vert_attrib = ShaderProgramDraw::get_generic_attrib_location("vertex");
    GLuint norm_attrib = ShaderProgramDraw::get_generic_attrib_location("normal");
    GLuint texcoord_attrib = ShaderProgramDraw::get_generic_attrib_location("texcoord");

    GLuint VAO, VBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    vector<Vec3f> interleave;
    for (int i = 0; i < triangles.size();i++)
    {
        interleave.push_back(triangles[i]);
        interleave.push_back(normals[i]);
    }

    glBufferData(GL_ARRAY_BUFFER,interleave.size()*sizeof(Vec3f),&interleave[0],GL_STATIC_DRAW);

    glVertexAttribPointer(vert_attrib, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), 0);
    glVertexAttribPointer(norm_attrib, 3, GL_FLOAT, GL_FALSE, 2*sizeof(Vec3f), reinterpret_cast<void*>(sizeof(Vec3f)));

    glVertexAttrib3f(texcoord_attrib, 0, 0,0);
    glEnableVertexAttribArray(vert_attrib);
    glEnableVertexAttribArray(norm_attrib);

    return VAO;
}

void Tree::draw(GLGraphics::ShaderProgramDraw & shader_prog) {


        const int N_max = 1000;
        static int N = 1;
        static GLuint vao = createVAO(tree.getTriangles(), tree.getNormals());
        static GLint count = tree.getTriStripLength();

    #if GENERATIONS
        static QTime generationTimer;
        static QTime updateTimer;
        if (updateTimer.isNull())
        {
            generationTimer.start();
            updateTimer.start();
        }

    #else
        static QTime age;
    #endif

        static Mat4x4f M[N_max];


        QQueue<Mat4x4f> branches = tree.getInstanceMatrices();


        int NN = branches.size();
        int iterations = (NN - 1) / 64 + 1;

        if(NN == 0)
            return;

        for(int it = 0; it < iterations; ++it)
        {
            N = it == iterations - 1 ? NN % 64 : 64;

            for (int i = 0; i < N; i++)
                M[i] = transpose(scaling_Mat4x4f(Vec3f(0.02))*branches[i + it * 64]);

            static Vec4f mat_diff[N_max];
            static bool was_here = false;
            if(!was_here)
            {
                was_here = true;
                for(int i=0; i<N_max; ++i)
                {
                    float xdisp = 3*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    float ydisp = 3*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    Vec3f p(4*(i%10)-12+xdisp, 4*((i/10))-12+ydisp,0.0);
                    p[2] = 4;//terra.height(p[0], p[1]);
        //            float scale = 0.009*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        //            float angle = 2*pi*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        //            M[i] = transpose(translation_Mat4x4f(p)*scaling_Mat4x4f(Vec3f(0.006 + scale))*rotation_Mat4x4f(ZAXIS, angle));
                    float brown = 0.15;//*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    float green = 0.01;//*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    float gray  = 0.01;//*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    mat_diff[i] = Vec4f(0.2+gray+brown*2,
                                        0.1+gray+brown+green,
                                        gray,1);
                }
            }

            shader_prog.set_model_matrix(identity_Mat4x4f());

            glBindVertexArray(vao);
            check_gl_error();
            glUniformMatrix4fv(shader_prog.get_uniform_location("InstanceMatrix"), N, GL_FALSE, (const GLfloat*) &M);
            check_gl_error();
            glUniform4fv(shader_prog.get_uniform_location("mat_diff"), N, (const GLfloat*) &mat_diff);
            check_gl_error();
            glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, count, N);
            check_gl_error();
        }

        //qDebug() << N << endl;
}

TreeData & Tree::getTreeData() {
    return tree;
}

