#ifndef SKYBOX_H
#define SKYBOX_H
#include "SceneMeshObject.h"
#include "../rendering/ShaderCache.h"
#include <CGLA/Mat4x4f.h>
#include <CGLA/Vec4f.h>
#include <GLGraphics/ShaderProgram.h>
#include <QTime>
#include <QTimer>
#include <string>
#include <vector>

class Skybox : SceneObject {
private:
    std::vector<std::string> images = {"left.png", "front.png", "right.png", "back.png", ""};
public:
};

#endif // SKYBOX_H
