/*
 *  terrain.h
 *  02564_Framework
 *
 *  Created by J. Andreas Bærentzen on 02/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __TERRAIN_H__
#define __TERRAIN_H__

#include <CGLA/Vec3f.h>
#include <GLGraphics/User.h>
#include <GLGraphics/ShaderProgram.h>
#include "SceneMeshObject.h"
#include <vector>
#include <stdlib.h>

#include <GLGraphics/GLHeader.h>

#include "../auxil.h"

/** This class represents a terrain. It is very simple: The terrain is synthesized using noise
 and once created we can query height and normal at a given 2D location or render the thing. */
class Terrain: public GLGraphics::HeightMap, public SceneMeshObject
{
	float A;
	float delta;
	
	float x0, y0;
    GLuint noise_tex;
    GLuint mesh;
    int num_mesh_vertices;
    void generateTerrain(GLGraphics::ShaderProgramDraw &shader_prog);
    void createNoiseTexture();
public:
	
	/// Construct terrain with amplitude A and scaling delta.
    Terrain(float _A, float _delta);
	
	/// Returns height at given 2D location
	float height(float _x, float _y) const;
	
	/// Returns normal at given 2D location
	CGLA::Vec3f normal(float x, float y) const;
	
	/// Draw using OpenGL: Assumes a reasonable shading program is used.
    virtual void draw(GLGraphics::ShaderProgramDraw&);
};

#endif
