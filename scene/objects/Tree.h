#ifndef TREE_H
#define TREE_H
#include "SceneMeshObject.h"
#include "../rendering/ShaderCache.h"
#include <CGLA/Mat4x4f.h>
#include <CGLA/Vec4f.h>
#include <GLGraphics/ShaderProgram.h>
#include <QTime>
#include <QTimer>
#include "../generation/TreeData.h"

class Tree : public SceneMeshObject {
    float spread;
    CGLA::Mat4x4f transform;
    CGLA::Vec4f mat_diff;
    TreeData tree;
public:
    Tree();
    virtual void draw(GLGraphics::ShaderProgramDraw&);
    void createTransform();
    void createMaterial();
    TreeData & getTreeData();
    virtual void update(float);
};


#endif // TREE_H
