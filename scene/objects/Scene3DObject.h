#ifndef SCENE3DOBJECT_H
#define SCENE3DOBJECT_H

#include <string>

#include <GLGraphics/ThreeDObject.h>

#include "../scene/SceneObject.h"
#include "../rendering/ShaderCache.h"

class Scene3DObject : public SceneObject {
    GLGraphics::ThreeDObject & object;
public:
    //Scene3DObject() {}
    Scene3DObject(GLGraphics::ThreeDObject & object, ShaderCache::Shaders shader) :
        SceneObject(shader), object(object){}
    GLGraphics::ThreeDObject & get3DObject() const {
        return object;
    }

    //Overrides from SceneObject
    virtual void update(float dt);
    virtual void draw(GLGraphics::ShaderProgramDraw& shader_prog);

    virtual void setPosition(const CGLA::Vec3f & position) {
        object.translate(position);
    }
//    virtual void setRotation(const CGLA::Vec3f & rotation) {
//        object.rotate(rotation);
//    }
//    virtual void setScale(const CGLA::Vec3f & scale) {
//        object.scaling_factors(scale);
//    }

//    virtual const CGLA::Vec3f & getPosition() const {
//        return object.scaling_factors;
//    }
//    virtual const CGLA::Vec3f & getRotation() const {
//        return rot;
//    }
//    virtual const CGLA::Vec3f & getScale() const {
//        return scale;
//    }
};


#endif // SCENE3DOBJECT_H
