/*
 *  terrain.cpp
 *  02564_Framework
 *
 *  Created by J. Andreas Bærentzen on 02/02/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#define SOLUTION_CODE

#include "Terrain.h"

#define USE_VERTEX_ARRAY 1

using namespace CGLA;
using namespace GLGraphics;
using namespace std;

Terrain::Terrain(float _A, float _delta):
    A(_A), SceneMeshObject(ShaderCache::GBufferRenderTerrain),
    delta(_delta), x0(12.231f), y0(7.91f), noise_tex(0), mesh(0), num_mesh_vertices(-1) {
}

float Terrain::height(float _x, float _y) const
{
    float x = delta*_x+x0;
    float y = delta*_y+y0;
    return A*turbulence(x,y);
}

Vec3f Terrain::normal(float x, float y) const 
{
    Vec3f xvec(2,0,(height(x+1,y)-height(x-1,y))/2.0);
    Vec3f yvec(0,2,(height(x,y+1)-height(x,y-1))/2.0);
    return normalize(cross(xvec, yvec));
}


void Terrain::createNoiseTexture() {
    int MMW = 1024;
    int MMH = 1024;
    // Create texture at this point
    float* noise = (float*)malloc(MMW*MMH*sizeof(float));
    float linenoise;

    for (int i = 0; i < MMW; i+= 4)
    {
        for (int j = 0; j < MMH; j+=4)
        {
            noise[i*MMH+j] = 0.3* static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            for (int i2 = 0; i2 < 4; i2++)
                for (int j2 = 0; j2 < 4; j2++)
                    noise[(i+i2)*MMH+(j+j2)] = (2-abs(i2-1.5))*noise[i*MMH+j];
        }
    }

    for (int i = 0; i < MMW; i++)
    {
        linenoise = 0.2*static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        for (int j = 0; j < MMH; j++)
        {
            noise[i*MMH+j] += linenoise + 0.6* static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        }
    }
    glGenTextures(1, &noise_tex);
    glBindTexture(GL_TEXTURE_2D, noise_tex);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, MMW, MMH, 0, GL_RED, GL_FLOAT, noise);
    glGenerateMipmap(GL_TEXTURE_2D);
}

void Terrain::generateTerrain(ShaderProgramDraw & shader_prog) {
    GLuint buffers[2];
    glGenVertexArrays(1, &mesh);
    glBindVertexArray(mesh);
    glGenBuffers(2, buffers);

    vector<Vec3f> vertices;
    vector<Vec3f> normals;

    for(int j=-30;j<30;++j)
    {
        normals.push_back(normal(-30,j+1));
        vertices.push_back(Vec3f(-30,j+1,height(-30,j+1)));
        for(int i=-30;i<30;++i)
        {
            normals.push_back(normal(i,j+1));
            vertices.push_back(Vec3f(i,j+1,height(i,j+1)));
            normals.push_back(normal(i,j));
            vertices.push_back(Vec3f(i,j,height(i,j)));
        }
        normals.push_back(normal(29,j));
        vertices.push_back(Vec3f(29,j,height(29,j)));
    }
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vec3f), &vertices[0], GL_STATIC_DRAW);
    GLuint vertex_attrib = shader_prog.get_attrib_location("vertex");
    glEnableVertexAttribArray(vertex_attrib);
    glVertexAttribPointer(vertex_attrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[1]);
    glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(Vec3f), &normals[0], GL_STATIC_DRAW);
    GLuint normal_attrib = shader_prog.get_attrib_location("normal");
    glEnableVertexAttribArray(normal_attrib);
    glVertexAttribPointer(normal_attrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

    num_mesh_vertices = vertices.size();
}

void Terrain::draw(ShaderProgramDraw& shader_prog)
{
    shader_prog.set_model_matrix(identity_Mat4x4f());
    shader_prog.use_texture(GL_TEXTURE_2D, "noise_tex", noise_tex);

    if(mesh == 0) {
        generateTerrain(shader_prog);
    } else {
        glBindVertexArray(mesh);
    }

    glDrawArrays(GL_TRIANGLE_STRIP, 0, num_mesh_vertices);
}
