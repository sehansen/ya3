#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H
#include <string>
#include <GLGraphics/ShaderProgram.h>
#include "../rendering/ShaderCache.h"
class SceneObject {
    ShaderCache::Shaders shader;
    ShaderCache::Shaders shadowShader;
    CGLA::Vec3f pos;
    CGLA::Vec3f rot;
    CGLA::Vec3f scale;
public:
    //SceneObject() : vertShader(std::move(std::string{})), fragShader(std::string()), geomShader(std::string()) {}
    SceneObject(ShaderCache::Shaders shader, CGLA::Vec3f pos=CGLA::Vec3f(0, 0, 0),
                CGLA::Vec3f rot=CGLA::Vec3f(0, 0, 0), CGLA::Vec3f scale=CGLA::Vec3f(1, 1, 1)) :
        shader(shader), pos(pos), rot(rot), scale(scale){}
    const ShaderCache::Shaders & getShader() const {
        return shader;
    }

    void setShader(ShaderCache::Shaders shader){
        this->shader = shader;
    }

    virtual void setPosition(const CGLA::Vec3f & position) {
        pos = position;
    }
    virtual void setRotation(const CGLA::Vec3f & rotation) {
        rot = rotation;
    }
    virtual void setScale(const CGLA::Vec3f & scale) {
        this->scale = scale;
    }

    virtual const CGLA::Vec3f & getPosition() const {
        return pos;
    }
    virtual const CGLA::Vec3f & getRotation() const {
        return rot;
    }
    virtual const CGLA::Vec3f & getScale() const {
        return scale;
    }

    virtual void update(float) {}
    virtual void draw(GLGraphics::ShaderProgramDraw& shader_prog) = 0;
};



#endif // SCENEOBJECT_H
