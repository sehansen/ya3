#include "FloatingCamera.h"

FloatingCamera::FloatingCamera(const CGLA::Vec4f& lightingPos):
    Camera(lightingPos), targetPosition(CGLA::Vec3f(0, 0, 7)), viewAngleX(M_PI/16),viewAngleY(M_PI/64),
    distance(10.0f), animationTime(3.0f), time(0.0f){
}

void FloatingCamera::update(float dt) {
    time += dt;
    float xTime = fmod(time, animationTime*3);
    float yTime = fmod(time, animationTime*5);
    float inclination = sin((yTime/(animationTime*5))*4*M_PI)*viewAngleY + M_PI/1.9f;
    float azimuth  = sin((xTime/(animationTime*3))*2*M_PI)*viewAngleX;
    setPosition(CGLA::Vec3f(sin(inclination)*cos(azimuth), sin(inclination)*sin(azimuth), cos(inclination))*distance + targetPosition);

    setDirection(targetPosition - getPosition());
}
