#ifndef CAMERA_H
#define CAMERA_H
#include <GLGraphics/GLHeader.h>
#include <GLGraphics/ThreeDObject.h>
#include <GLGraphics/ShaderProgram.h>
#include <GLGraphics/User.h>
#include <QEvent>
#include <QKeyEvent>
#include <GLGraphics/ShaderProgram.h>
#include <cmath>
#include <CGLA/ArithVec3Float.h>
class Camera {

    CGLA::Vec3f pos;
    CGLA::Vec3f dir;
    int width;
    int height;
    CGLA::Vec4f lightingPos;
public:
    Camera(const CGLA::Vec4f & lightingPos) :
        pos(-7.60765f, 14.907f, 4.37377f), dir(normalize(CGLA::Vec3f(1,1,0))),
        width(0), height(0), lightingPos(lightingPos) {}

    void setPosition(const CGLA::Vec3f& pos) {
        this->pos = pos;
    }

    void movePosition(const CGLA::Vec2f & to) {
        //std::cout << "x: "<<getPosition()[0] << " y: "<< getPosition()[1] << " z: "<< getPosition()[2] << " to: "<<to[0]<<","<<to[1]<< std::endl;

        //Stay at ground level
        //pos += to[0] * normalize(CGLA::Vec3f(dir[0],dir[1],0));
        //pos += to[1] * normalize(CGLA::Vec3f(-dir[1],dir[0],0));

        //Free fly
        pos += dir * to[0];
        auto strafeDir = -normalize(CGLA::cross(dir, CGLA::Vec3f(0,0,1)));
        pos += strafeDir * to[1];

    }

    void setDirection(const CGLA::Vec3f& dir) {
        this->dir = dir;
    }

    //Look in a different direction
    void changeDirection(const CGLA::Vec3f& dir) {
        this->dir = normalize(this->dir + dir);
    }


    //Look at a target point
    void lookAt(const CGLA::Vec3f& targetPos) {
        dir = normalize(targetPos - pos);
    }

    const CGLA::Vec3f & getPosition() const {
//        std::cout << pos[0] <<" "<< pos[1]<<" " << pos[2] << std::endl;
        return pos;
    }

    const CGLA::Vec3f& getDirection() const {
        return dir;
    }

    int getWidth() const {
        return width;
    }
    int getHeight() const {
        return height;
    }

    CGLA::Vec4f getLightingPost() const {
        return lightingPos;
    }

    void setDimension(int width, int height) {
        this->width = width;
        this->height = height;
        glViewport(0, 0, width, height);
    }

    void setLightingPost(CGLA::Vec4f lightingPos) {
        this->lightingPos = lightingPos;
    }

    const CGLA::Vec2f getRotations() const;

    void setLightAndCamera(GLGraphics::ShaderProgramDraw&) const;

    void event(QEvent *event);

    CGLA::Mat4x4f getViewMatrix() const
    {
        return lookat_Mat4x4f(pos, dir, CGLA::Vec3f(0,0,1));
    }
    virtual void update(float) {
    }
protected:
    virtual void mousePressEvent(QMouseEvent *){}
    virtual void mouseReleaseEvent(QMouseEvent *){}
    virtual void mouseMoveEvent(QMouseEvent *){}
    virtual void keyPressEvent(QKeyEvent *){}
    virtual void keyReleaseEvent(QKeyEvent *){}
};


#endif // CAMERA_H
