#include "Scene.h"

Scene::Scene(const QGLFormat &format, QWidget *parent) :
    QGLWidget( new Core3_2_context(format), parent), time(nullptr), lastFrameTime(0.0f),
    renderer(nullptr), camera(nullptr), objects(), trees(), inputHelper(nullptr)
{
    camera = new MovableCamera(CGLA::Vec4f(.3f,.3f,1,0), *this);
    //camera = new FloatingCamera(CGLA::Vec4f(.3f,.3f,1,0));

    setRenderer(new ToonRenderer(*camera));

    time = new QTime();
    lastFrameTime =time->elapsed();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(4);

    // Input
    inputHelper = new KeyboardHelper();
    inputHelper->Init();
}

void Scene::setupScene() {
    objects.push_back(new Terrain(30,0.025f));
    Tree * t1 = new Tree();
    t1->setScale(CGLA::Vec3f(0.01f, 0.01f, 0.01f));
    t1->setPosition(CGLA::Vec3f(5, 5, 3));
    objects.push_back(t1);
    trees.push_back(t1);


    camera->lookAt(t1->getTreeData().getPosition());

}

void Scene::paintGL() {
    renderer->render(objects);
}

void Scene::update() {
    if (time->isNull()) {
        time->start();
    }
    float deltaTime = (time->elapsed() - lastFrameTime)/1000.0f; //In seconds
    lastFrameTime = time->elapsed();
    camera->update(deltaTime);
    updateInteraction(deltaTime);
    for (auto const & obj : objects) {
        obj->update(deltaTime);
    }
    inputHelper->Update(std::min(0.2f, deltaTime));
    repaint();
}

void Scene::initializeGL() {

    renderer->initializeGL();
    ShaderCache::init();
    setupScene();
}

void Scene::resizeGL( int width, int height ) {
    camera->setDimension(width, height);
}

void Scene::setRenderer(Renderer * newRenderer) {
    delete renderer;
    renderer = newRenderer;
}

Scene::~Scene() {
    delete renderer;
    delete camera;
}

InputHelper & Scene::getInputHelper() {
    return *inputHelper;
}

//Input
void Scene::mousePressEvent(QMouseEvent *m) {
}
void Scene::mouseReleaseEvent(QMouseEvent *m) {
}
void Scene::mouseMoveEvent(QMouseEvent *m) {
}
void Scene::keyPressEvent(QKeyEvent *keyEvent) {
}
void Scene::keyReleaseEvent(QKeyEvent *keyEvent) {
    Qt::Key key = static_cast<Qt::Key>(keyEvent->key());
    switch(key) {
        case Qt::Key_R:
            ShaderCache::reloadShaders();
        break;
    }
}


bool Scene::event( QEvent * event ) {
    camera->event(event);
    QWidget::event(event);
    return true;
}

void Scene::updateInteraction(float timeStep)
{
    InputHelper * m_inputHelper = &getInputHelper();
    TreeData tree = trees[0]->getTreeData();
    m_inputHelper->Update(timeStep);



    float tilt = 0.7f * m_inputHelper->TorsoTiltAngle();
    float tiltfalloff = 0.5;
    if(tilt > -20 && tilt < 20)
        tree.TiltInteraction(camera->getDirection(), tilt, tiltfalloff);


    float spread = 1.5f * m_inputHelper->HandToShoulderDistance(YA3Side_Left) * m_inputHelper->GetBranchingAngle(YA3Side_Left);
    float spreadfalloff = 0.5f;
    if(spread > -20 && spread < 20)
        tree.SpreadInteraction(camera->getDirection(), spread, 1, spreadfalloff, YA3Side_Left);

    spread = 1.5f * m_inputHelper->HandToShoulderDistance(YA3Side_Right) * m_inputHelper->GetBranchingAngle(YA3Side_Right);
    spreadfalloff = 0.5f;
    if(spread > -20 && spread < 20)
        tree.SpreadInteraction(camera->getDirection(), spread, 1, spreadfalloff, YA3Side_Right);


    float length = m_inputHelper->HandToShoulderDistance3D(YA3Side_Left) - 0.4f;
    float lengthFalloff = 0.9f;
    if(length > -20 && length < 20)
        tree.LengthInteraction(camera->getDirection(), length, 1, lengthFalloff, YA3Side_Left);

    length = m_inputHelper->HandToShoulderDistance3D(YA3Side_Right) - 0.4f;
    lengthFalloff = 0.9f;
    if(length > -20 && length < 20)
        tree.LengthInteraction(camera->getDirection(), length, 1, lengthFalloff, YA3Side_Right);

}
