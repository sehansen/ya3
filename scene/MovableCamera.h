#ifndef MOVABLE_CAMERA_H
#define MOVABLE_CAMERA_H
#include <Qt>
#include "Camera.h"
#include <GLGraphics/GLHeader.h>
#include <unordered_map>

class MovableCamera : public Camera {
    static const float MOVE_SPEED;
    static const float STRAFE_SPEED;
    static const float LOOK_SPEED;
    Qt::Key forward;
    Qt::Key backward;
    Qt::Key strafeLeft;
    Qt::Key strafeRight;
    Qt::MouseButton lookButton;
    Qt::Key resetPosition;
    bool isLooking;
    CGLA::Vec2i mousePressPos;
    QGLWidget & window;
    std::unordered_map<Qt::Key, bool> pressedKeys;

    bool checkIfKeyIsPressed(Qt::Key key) {
        return (pressedKeys.count(key) && pressedKeys[key]);
    }

public:
    MovableCamera(const CGLA::Vec4f & lightingPos, QGLWidget & window);
    void setKeyForward(Qt::Key forward) {
        this->forward = forward;
    }
    void setKeyBackward(Qt::Key backward) {
        this->backward = backward;
    }
    void setKeyStrafeLeft(Qt::Key strafeLeft) {
        this->strafeLeft = strafeLeft;
    }
    void setKeyStrafeRight(Qt::Key strafeRight) {
        this->strafeRight = strafeRight;
    }
    void setLookMouseButton(Qt::MouseButton lookButton) {
        this->lookButton = lookButton;
    }


    Qt::Key getKeyForward() const {
        return forward;
    }
    Qt::Key getKeyBackward() const {
        return backward;
    }
    Qt::Key getKeyStrafeLeft() const {
        return strafeLeft;
    }
    Qt::Key getKeyStrafeRight() const {
        return strafeRight;
    }
    Qt::MouseButton getLookMouseButton() const {
        return lookButton;
    }
    virtual void update(float);
protected:
    virtual void mousePressEvent(QMouseEvent *);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void keyPressEvent(QKeyEvent *);
    virtual void keyReleaseEvent(QKeyEvent *);
};


#endif //END MOVABLE_CAMERA_H
