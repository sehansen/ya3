#include "soundsystem.h"

SoundSystem::SoundSystem(QObject *parent) :
    QObject(parent)
{
    pulsesMusic.setSource(QUrl::fromLocalFile(QCoreApplication::arguments()[1] + QString("\\data\\music for 18 musicians - 01.wav")));
    pulsesMusic.setLoopCount(QSoundEffect::Infinite);
    pulsesMusic.setVolume(0.05f);
    pulsesMusic.play();
}
