#ifndef FLAGS_H
#define FLAGS_H

#define GENERATIONS (0)
#define CONESIZE (5)
#define INTERLEAVED (1)
#define INSTANCED (1) // You need to change the shaders manually

/* ========= SOUND ========= */
#define MUTE_SOUND


/* ========= INPUT ========= */
//#define USE_KINECTHELPER
#define USE_KEYBOARDHELPER


#endif // FLAGS_H
