#version 150

#define TOON (1)

vec4 _color;

uniform vec4 mat_diff;
uniform vec4 mat_spec;
uniform float mat_spec_exp;
uniform sampler2D tex;
uniform vec4 light_amb;
uniform vec4 light_diff;
uniform vec4 light_pos;
uniform vec4 light_spec;
uniform vec3 light_half_vec;

in vec3 p;
in vec3 n;
in vec3 t;

out vec4 fragData[3];

void main() 
{
        fragData[0].rgb = p;
        fragData[1].rgb = normalize(n);
        float d = dot(fragData[1].rgb,normalize(light_pos.xyz));
        _color = mat_diff*light_amb;
#if TOON
        if (d>-0)
        {
            _color += mat_diff*0.5*light_diff;
            float n_dot_h = dot(fragData[1].rgb, normalize(light_half_vec));
            if(n_dot_h>0.9)
                _color += mat_spec*light_spec;
        }
#else
        if (d>0)
        {
            _color += mat_diff*d*light_diff;
            float n_dot_h = dot(fragData[1].rgb, normalize(light_half_vec));
            if(n_dot_h>0)
                _color += mat_spec*pow(n_dot_h, mat_spec_exp)*light_spec;
        }
#endif
//        fragData[2] = _color*texture(tex, t.xy);

        fragData[2] = texture(tex, t.xy);
        fragData[2].w = length(mat_spec.xyz);
}
