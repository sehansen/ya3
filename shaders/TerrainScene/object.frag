#version 150

uniform sampler2D tex;  // Uniform specifying the texture unit

vec4 _color;

in vec3 _texcoord;
in vec3 _normal;

uniform mat3 N;
uniform vec4 light_pos;
uniform vec3 light_half_vec;
uniform vec4 light_diff;
uniform vec4 light_spec;
uniform vec4 light_amb;
uniform vec4 mat_diff;
uniform vec4 mat_spec;
uniform float mat_spec_exp;

out vec4 fragColor;

#ifdef asdfas
vec4 texture(sampler2DRect a,vec4 b);
#endif

void main()
{
    vec3 norm = normalize(N*_normal);
    float d = dot(norm,normalize(light_pos.xyz));
    _color = mat_diff*light_amb;
    if(d>0.0)
    {
        _color += mat_diff*d*light_diff;
        float n_dot_h = dot(norm, normalize(light_half_vec));
        if(n_dot_h>0)
            _color += mat_spec*pow(n_dot_h, mat_spec_exp)*light_spec;
    }

    fragColor = _color * texture(tex, _texcoord.xy);
}
