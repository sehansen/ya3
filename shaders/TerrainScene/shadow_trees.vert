#version 150

#define INSTANCED (1)
in vec3 vertex;
uniform mat4 PVM;

#if INSTANCED
in int instanceID;
uniform mat4 InstanceMatrix[100];
#else
uniform mat4 InstanceMatrix;
#endif
void main()
{
#if INSTANCED
    gl_Position = (PVM) * InstanceMatrix[instanceID] * vec4(vertex,1);
#else
    gl_Position = (PVM) * InstanceMatrix * vec4(vertex,1);
#endif
}

