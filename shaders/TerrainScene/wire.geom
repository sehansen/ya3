#version 150
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;


uniform vec2 WIN_SCALE;           // precisely equal to: WINDOW_WIDTH/2.0, WINDOW_HEIGHT/2.0
noperspective out vec3 dist;  // Vec3 containing the distances from a vertex to opposite edge

float pldist(vec4 p, vec4 l1, vec4 l2)
{
    vec3 other = l1.xyz - l2.xyz;
    vec3 to = p.xyz - l1.xyz;

    return length(to - other*dot(other, to));
}

void main(void)
{
    vec4 g0 = gl_in[0].gl_Position; g0 = g0/g0.w;
    vec4 g1 = gl_in[1].gl_Position; g1 = g1/g1.w;
    vec4 g2 = gl_in[2].gl_Position; g2 = g2/g2.w;

    vec3 other = g1.xyz/g1.w - g2.xyz/g2.w;
    vec3 to = g0.xyz/g0.w - g1.xyz/g1.w;

    //dist = vec3(50,0,0);
    //dist = vec3(length(WIN_SCALE)*length(to - other*dot(other, to)),0,0);
    //dist = vec3(length(WIN_SCALE)*length(g0.xyz - 0.5*g1.xyz - 0.5*g2.xyz),0,0);

    dist = vec3(length(WIN_SCALE)*pldist(g0,g1,g2),0,0);
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    //dist = vec3(0,50,0);
    dist = vec3(0,length(WIN_SCALE)*pldist(g1,g2,g0),0);
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    //dist = vec3(0,0,50);
    dist = vec3(0,0,length(WIN_SCALE)*pldist(g2,g0,g1));
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();
}
