#version 150

uniform sampler2D tex;  // Uniform specifying the texture unit

// remember to change the size of mat_diff when you change the number of trees
#define INSTANCED (1)

vec4 _color;

in vec3 _texcoord;
in vec4 _normal;

uniform mat3 N;
uniform vec4 light_pos;
uniform vec3 light_half_vec;
uniform vec4 light_diff;
uniform vec4 light_spec;
uniform vec4 light_amb;
#if INSTANCED
flat in int instanceID;
uniform vec4 mat_diff[128];
#else
uniform vec4 mat_diff;
#endif
uniform vec4 mat_spec;
uniform float mat_spec_exp;

out vec4 fragColor;

void main()
{

    vec3 norm = normalize(N*_normal.xyz);
    float d = dot(norm,normalize(light_pos.xyz));


#if INSTANCED
    _color = mat_diff[instanceID]*light_amb;
#else
    _color = mat_diff*light_amb;
#endif
    if(d>0.0)
    {
#if INSTANCED
        _color += mat_diff[instanceID]*d*light_diff;
#else
        _color += mat_diff*d*light_diff;
#endif
        float n_dot_h = dot(norm, normalize(light_half_vec));
        if(n_dot_h>0)
            _color += mat_spec*pow(n_dot_h, mat_spec_exp)*light_spec;
    }

    fragColor = _color * texture(tex, _texcoord.xy);
}
