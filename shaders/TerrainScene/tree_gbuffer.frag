#version 150

// remember to change the size of mat_diff when you change the number of trees
#define INSTANCED (1)
#define TOON (1)

vec4 _color;
\
in vec4 _normal;
in vec3 p;

uniform mat3 N;
uniform vec4 light_pos;
uniform vec3 light_half_vec;
uniform vec4 light_diff;
uniform vec4 light_spec;
uniform vec4 light_amb;
#if INSTANCED
flat in int instanceID;
uniform vec4 mat_diff[100];
#else
uniform vec4 mat_diff;
#endif
uniform vec4 mat_spec;
uniform float mat_spec_exp;

out vec4 fragData[3];

void main()
{

    fragData[0].rgb = p;
    fragData[1].rgb = normalize(N*_normal.xyz);
#if INSTANCED
    fragData[2] = mat_diff[instanceID];
#else
    fragData[2] = mat_diff;
#endif
    fragData[2].w = length(mat_spec.xyz);
}
