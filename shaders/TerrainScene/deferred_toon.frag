#version 150

uniform sampler2DRect gtex; 
uniform sampler2DRect ntex;
uniform sampler2DRect ctex;

uniform vec4 light_pos;
uniform vec3 light_half_vec;
uniform vec4 light_diff;
uniform vec4 light_spec;
uniform vec4 light_amb;

float sqr(float x) {return x*x;}

#ifdef asdfas
vec4 texture(sampler2DRect a,vec4 b);
#endif

vec4 _color;

out vec4 fragColor;
void main()
{
        float z = texture(gtex, gl_FragCoord.xy).z;
        float zedge = abs(texture(gtex, gl_FragCoord.xy + vec2(-1,0)).z + texture(gtex, gl_FragCoord.xy + vec2(1,0)).z + texture(gtex, gl_FragCoord.xy + vec2(0,-1)).z + texture(gtex, gl_FragCoord.xy + vec2(0,1)).z -4*z);

        vec4 base_color = texture(ctex, gl_FragCoord.xy);
        float mat_spec = base_color.w;
        float d = dot(texture(ntex, gl_FragCoord.xy).xyz,normalize(light_pos.xyz));
        _color = light_amb;
        if (d>-0)
        {
            _color += 0.4*light_diff;
            if (d>0.6)
                _color += 0.4*light_diff;
        }

        if (zedge > 0.1)
            fragColor = _color*base_color*max((1-0.3*(zedge)),0)*exp(-z*z/300) + vec4(0.9,0.9,.9,0)*(1-exp(-z*z/300));
        else if(z == -1000.0)
            fragColor  = vec4(0.9,0.9,.9,0);
        else
            fragColor = _color*base_color*exp(-z*z/300) + vec4(0.9,0.9,.9,0)*(1-exp(-z*z/300));
}
