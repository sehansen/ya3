#version 150

uniform sampler2DRect gtex; 
uniform sampler2DRect ntex;
uniform sampler2DRect ctex;
uniform vec3 disc_points[256];
uniform vec2 win_dim;

uniform int NO_DISC_POINTS;
const float RADIUS = 3.0;
const float WIN_RADIUS = 30.0;
const float WIN_SCALING = 2.0;

#ifdef asdfas
vec4 texture(sampler2DRect a,vec4 b);
#endif

float sqr(float x) {return x*x;}
out vec4 fragColor;
void main()
{
    const float pi = 3.1415926;
    vec3 p = texture(gtex, WIN_SCALING*gl_FragCoord.xy).xyz;
    if(p.z == -1000.0)
        fragColor = vec4(1.0);
    else
    {
        vec3 n = texture(ntex, WIN_SCALING*gl_FragCoord.xy).xyz;
        float ambient_occlusion=0.0;

        float A = 0;
        for (int i = 0; i < NO_DISC_POINTS; i++)
        {
            vec3 vi =  texture(gtex, WIN_SCALING*gl_FragCoord.xy + WIN_RADIUS*disc_points[i].xy).xyz - p;
            A -= max(0,1 - pow(length(vi)/RADIUS,2)) * max(0,dot(normalize(vi), n));
        }
        A = A / NO_DISC_POINTS + 1;
        fragColor = vec4(1,1,1,0)*abs(A);
    }
}
