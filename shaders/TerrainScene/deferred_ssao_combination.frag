#version 150

uniform sampler2DRect gtex;
uniform sampler2DRect ntex;
uniform sampler2DRect ctex;
uniform sampler2DRect ssao_tex;
uniform sampler2DShadow	shadow_map;
//uniform sampler2D	shadow_map;

uniform vec4 light_pos;
uniform vec3 light_half_vec;
uniform vec4 light_diff;
uniform vec4 light_spec;
uniform vec4 light_amb;
uniform vec4 mat_diff;
uniform float mat_spec_exp;

uniform mat4 Mat;
const float INV_WIN_SCALING = 0.5;

vec4 _color;

#ifdef asdfas
vec4 texture(sampler2DRect a,vec4 b);
#endif


float sample_shadow_map(vec3 eye_pos)
{
    return texture(shadow_map, (Mat*vec4(eye_pos,1)).xyz);
}

out vec4 fragColor;
void main()
{
    vec3 p = texture(gtex, gl_FragCoord.xy).xyz;
    vec4 base_color = texture(ctex, gl_FragCoord.xy);

    vec4 mat_spec = vec4(base_color.w);

    vec3 norm = texture(ntex, gl_FragCoord.xy).xyz;
    float d = dot(norm,normalize(light_pos.xyz));
    _color = light_amb*texture(ssao_tex, gl_FragCoord.xy/2);

    if(d>0.0)
    {
        _color += d*light_diff;
        float n_dot_h = dot(norm, normalize(light_half_vec));
        if(n_dot_h>0)
        {
            _color += 0.1*mat_spec*pow(n_dot_h, 20)*light_spec;
        }
    }
    if(p.z == -1000.0)
    {
        fragColor  = vec4(0.4,0.35,0.95,0);
    }
    else
    {
        float shadow = (sample_shadow_map(p + norm/50));
        if (shadow < 0.5)
            fragColor = 0.2*_color* texture(ctex, gl_FragCoord.xy);
        else
            fragColor = _color* texture(ctex, gl_FragCoord.xy);
//        fragColor = shadow*texture(ssao_tex, gl_FragCoord.xy/2)*vec4(1,1,1,0);//*_color* texture(ctex, gl_FragCoord.xy);
    }
}
