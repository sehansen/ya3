#version 150
in vec3 vertex;
in vec3 normal;
in vec3 texcoord;


uniform mat4 PVM;

out vec3 _normal;
out vec3 _texcoord;

void main()
{
    _normal = normal;
    _texcoord = texcoord;
    gl_Position = PVM * vec4(vertex,1);
}
