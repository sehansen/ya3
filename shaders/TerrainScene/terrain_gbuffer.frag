#version 150

uniform sampler2D noise_tex;
uniform vec4 light_diff;
uniform vec4 light_amb;
uniform vec4 light_pos;

#define TOON (0)

in vec3 p;
in vec3 pos;
in vec3 n;
in vec3 t;

const vec4 col_low = vec4(0.2,0.5,0.4,1.0);
const vec4 col_high = vec4(0.8,0.75,0.5,1.0);

out vec4 fragData[3];
void main() 
{
        fragData[0].rgb = p;
        fragData[1].rgb = normalize(n);
        // Map height to color

        float h = 0.4 * texture(noise_tex, 0.1*pos.xy).r;
        vec4 base_color = (1-h)*col_low + h*col_high;

        base_color.w = 0.0;
        float diff = dot(fragData[1].rgb, normalize(light_pos.xyz));


        fragData[2] = base_color;
}
