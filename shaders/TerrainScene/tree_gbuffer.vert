#version 150
in vec3 vertex;
in vec3 normal;
in vec3 texcoord;

// remember to change the size of InstanceMatrix when you change the number of trees
#define INSTANCED (1)

uniform mat4 PVM;
uniform mat4 VM;

#if INSTANCED
in int gl_InstanceID;
uniform mat4 InstanceMatrix[100];
flat out int instanceID;
#else
uniform mat4 InstanceMatrix;
#endif

out vec3 p;
//out vec3 pos;
out vec4 _normal;
out vec3 _texcoord;

void main()
{
    _texcoord = texcoord;
#if INSTANCED
    _normal = InstanceMatrix[gl_InstanceID] * vec4(normal,0);
    gl_Position = (PVM) * InstanceMatrix[gl_InstanceID] * vec4(vertex,1);
    instanceID = gl_InstanceID;
    p = (VM * InstanceMatrix[gl_InstanceID] * vec4(vertex, 1)).xyz;
#else
    _normal = InstanceMatrix * vec4(normal,0);
    gl_Position = (PVM) * InstanceMatrix * vec4(vertex,1);
    p = (VM * InstanceMatrix * vec4(vertex, 1)).xyz;
#endif
}
