#ifndef SHADERCACHE_H
#define SHADERCACHE_H

#include <GLGraphics/ShaderProgram.h>
#include <string>
#include <unordered_map>

namespace ShaderCache {
    enum Shaders {
        ToonShader,
        GBufferRenderObject,
        GBufferRenderTree,
        GBufferRenderTerrain
    };
    const std::string SHADER_PATH = "./shaders/TerrainScene/";
    void addShader(const Shaders &name, const std::string &vertShader, const std::string &fragShader, const std::string &geomShader="");
    GLGraphics::ShaderProgramDraw& getShader(const Shaders);
    void reloadShaders();
    void init();
}


#endif // SHADERCACHE_H
