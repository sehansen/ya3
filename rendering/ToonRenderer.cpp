#include "ToonRenderer.h"

void ToonRenderer::render(std::vector<SceneObject*> & objects) {
    Camera & camera = *getCamera();
    static GBuffer gbuffer(camera.getWidth(), camera.getHeight());
    GLGraphics::ShaderProgramDraw & toonShader = ShaderCache::getShader(ShaderCache::Shaders::ToonShader);
    //static ShaderProgramDraw deferred_shading(shader_path, "deferred.vert", "", "deferred_toon.frag");


    check_gl_error();
    gbuffer.rebuild_on_resize(camera.getWidth(), camera.getHeight());

    renderToGbuffer(gbuffer, objects);

    check_gl_error();
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDrawBuffer(GL_BACK);

    glDisable(GL_DEPTH_TEST);

    glClear(GL_COLOR_BUFFER_BIT);
    toonShader.use();
    getCamera()->setLightAndCamera(toonShader);

    gbuffer.bind_textures(0, 1, 2);
    toonShader.set_uniform("gtex", 0);
    toonShader.set_uniform("ntex", 1);
    toonShader.set_uniform("ctex", 2);

    check_gl_error();
    drawScreenAlignedQuad(toonShader);
    glEnable(GL_DEPTH_TEST);
}
