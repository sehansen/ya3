#include "Renderer.h"


void Renderer::setCamera(Camera * camera) {
    this->camera = camera;
}

Camera * Renderer::getCamera() const {
    return camera;
}

void Renderer::initializeGL() const {
    setup_gl();
    glClearColor( 0.7f, 0.7f, 0.7f, 0.0f );
    glEnable(GL_DEPTH_TEST);
}


void Renderer::renderToGbuffer(GBuffer& gbuffer, std::vector<SceneObject*> &objects)
{
//    GLGraphics::ShaderProgramDraw & gbufferObjects = ShaderCache::getShader(ShaderCache::Shaders::GBufferRenderObject);
//    GLGraphics::ShaderProgramDraw & gbufferTrees = ShaderCache::getShader(ShaderCache::Shaders::GBufferRenderTree);
//    GLGraphics::ShaderProgramDraw & gbufferTerrain = ShaderCache::getShader(ShaderCache::Shaders::GBufferRenderTerrain);
    gbuffer.enable();
    for(auto const & obj : objects) {
        GLGraphics::ShaderProgramDraw & shader = ShaderCache::getShader(obj->getShader());
        shader.use();
        getCamera()->setLightAndCamera(shader);
        obj->draw(shader);
    }
//    gbufferTerrain.use();
//    getCamera()->setLightAndCamera(gbufferTerrain);
//    t.draw(gbufferTerrain);

    //gbufferObjects.use();

    /*getCamera()->setLightAndCamera(gbufferObjects);
    draw_objects(gbufferObjects);

    gbufferTrees.use();

    getCamera()->setLightAndCamera(gbufferTrees);

    draw_trees(gbufferTrees, 60*m_inputHelper->GetHandDistance());*/
}

void Renderer::drawScreenAlignedQuad(GLGraphics::ShaderProgram& shader_prog)
{
    const CGLA::Vec2f points[] = {CGLA::Vec2f(-1,-1), CGLA::Vec2f(1,-1), CGLA::Vec2f(-1,1), CGLA::Vec2f(1,1)};
    GLuint pos_attrib = shader_prog.get_attrib_location("vertex");
    static GLuint VAO = 0;
    if(VAO == 0)
    {
        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);
        GLuint VBO;
        glGenBuffers(1, &VBO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER,4*sizeof(CGLA::Vec2f),&points[0],GL_STATIC_DRAW);
        glVertexAttribPointer(pos_attrib,2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(pos_attrib);
    }
    check_gl_error();
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLE_STRIP,0,4);
}
