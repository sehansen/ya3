#ifndef TOONRENDERER_H
#define TOONRENDERER_H
#include "../rendering/Renderer.h"
#include "../rendering/ShaderCache.h"
#include "../rendering/GBuffer.h"
#include "../scene/SceneObject.h"
#include "../scene/SceneObject.h"
#include <GLGraphics/ShaderProgram.h>
#include <vector>
#include <string>
#include <GLGraphics/GLHeader.h>
#include <CGLA/Vec2f.h>

class ToonRenderer : public Renderer {
public:
    ToonRenderer(Camera &camera) : Renderer(camera) {}
    virtual void render(std::vector<SceneObject*> &);
};

#endif // TOONRENDERER_H
