#ifndef RENDERER_H
#define RENDERER_H
#include <vector>

#include <GLGraphics/GLHeader.h>
#include <GLGraphics/ThreeDObject.h>
#include <GLGraphics/ShaderProgram.h>

#include "../scene/Camera.h"
#include "../scene/SceneObject.h"
#include "../rendering/GBuffer.h"


class Renderer {
    Camera * camera;
public:
    Renderer(Camera & camera) : camera(&camera){}
    virtual void render(std::vector<SceneObject*> & object) {}
    Camera * getCamera() const;
    void setCamera(Camera *);

    void renderToGbuffer(GBuffer& gbuffer, std::vector<SceneObject*> & objects);
    void drawScreenAlignedQuad(GLGraphics::ShaderProgram& shader_prog);

    //Init the graphics
    void initializeGL() const;
};


#endif // RENDERER_H
