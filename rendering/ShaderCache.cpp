#include "ShaderCache.h"
using namespace ShaderCache;
namespace ShaderCache {
    namespace {
        std::unordered_map<Shaders, GLGraphics::ShaderProgramDraw*> cache;
    }
    //const std::string SHADER_PATH = "./shaders/TerrainScene/";
    void addShader(const Shaders &name, const std::string &vertShader, const std::string &fragShader, const std::string &geomShader) {
        cache[name] = new GLGraphics::ShaderProgramDraw(SHADER_PATH, vertShader, geomShader, fragShader);

    }

    GLGraphics::ShaderProgramDraw & getShader(const Shaders name) {
        return *cache[name];
    }

    void reloadShaders() {
        for(auto kv : cache) {
            kv.second->reload();
        }
    }

    void init() {
        //deferred_shading(shader_path, "deferred.vert", "", "deferred_toon.frag");
        //addShader("ToonShader")
        //static ShaderProgramDraw objects_render_to_gbuffer(shader_path, "objects_gbuffer.vert", "", "objects_gbuffer.frag");
        //static ShaderProgramDraw terrain_render_to_gbuffer(shader_path, "terrain_gbuffer.vert", "", "terrain_gbuffer.frag");
        //static ShaderProgramDraw tree_render_to_gbuffer(shader_path, "tree_gbuffer.vert", "", "tree_gbuffer.frag");

        addShader(Shaders::ToonShader, "deferred.vert", "deferred_toon.frag");
        addShader(Shaders::GBufferRenderObject, "objects_gbuffer.vert", "objects_gbuffer.frag");
        addShader(Shaders::GBufferRenderTerrain, "terrain_gbuffer.vert", "terrain_gbuffer.frag");
        addShader(Shaders::GBufferRenderTree, "tree_gbuffer.vert", "tree_gbuffer.frag");
        reloadShaders();
    }
}
