#include "turtle.h"

// Produce the mesh for a truncated cone
void truncated_cone(const Mat4x4f& m,           // transformation matrix used for the points
                    float l,                    // length of truncated cone
                    float w0,                   // width at base
                    float w1,                   // width at top
                    QQueue<Vec3f>& triangles,   // triangles (out)
                    QQueue<Vec3f>& normals)     // normals (out)
{
    float len = sqrt(l*l + sqr(w0-w1));
    float a = l/len;
    float b = (w0-w1)/len;
    const int N = CONESIZE;
    for(int i=0;i<=N;++i)
    {
        float alpha = 2.0*M_PI*float(i)/N;
        Vec3f p0 = m.mul_3D_point(Vec3f(w0*cos(alpha), w0*sin(alpha), 0));
        Vec3f p1 = m.mul_3D_point(Vec3f(w1*cos(alpha), w1*sin(alpha), l));
        Vec3f n0 = m.mul_3D_vector(a*Vec3f(cos(alpha), sin(alpha), b));
        Vec3f n1 = n0;

        alpha = 2.0*M_PI*float(i+1)/N;
        Vec3f p2 = m.mul_3D_point(Vec3f(w0*cos(alpha), w0*sin(alpha), 0));
        Vec3f p3 = m.mul_3D_point(Vec3f(w1*cos(alpha), w1*sin(alpha), l));
        Vec3f n2 = m.mul_3D_vector(a*Vec3f(cos(alpha), sin(alpha), b));
        Vec3f n3 = n2;

        if ( i == 0)
        {
            normals.push_back(n0);
            triangles.push_back(p0);
        }

        normals.push_back(n0);
        triangles.push_back(p0);

        normals.push_back(n1);
        triangles.push_back(p1);

        if ( i == N)
        {

            normals.push_back(n1);
            triangles.push_back(p1);
        }
    }
}

void interpret(QQueue<LSElement> str, float w0,
               QQueue<CGLA::Vec3f>& triangles, QQueue<CGLA::Vec3f>& normals)
{
    Turtle turtle(w0);
    for(int i=0;i<str.size(); ++i)
    {
        LSElement elem = str[i];
        switch(elem.symbol)
        {
        case LS_DRAW:
            truncated_cone(turtle.get_transform(), elem.datum1,
                           turtle.get_width(), turtle.get_width()*0.65,
                           triangles, normals);
            turtle.move(elem.datum1);
            break;
        case LS_WIDTH:
            turtle.set_width(elem.datum1);
            break;
        case LS_LEFT_BRACKET:
            turtle.push();
            break;
        case LS_TURN:
            turtle.turn(elem.datum1);
            break;
        case LS_RIGHT_BRACKET:
            turtle.pop();
            break;
        case LS_ROLL:
            turtle.roll(elem.datum1);
            break;
            // Implement the other cases
        }
    }
}

bool Rule::apply(const LSElement elem, std::vector<LSElement>& out)
{
    if(elem.symbol == LS_A)
    {
        float s = elem.datum1;
        float w = elem.datum2;

        if(s>=smin)
        {
            out.push_back(LSElement(LS_WIDTH, w));
            out.push_back(LSElement(LS_DRAW, s));
            out.push_back(LSElement(LS_LEFT_BRACKET));
            out.push_back(LSElement(LS_TURN, alpha1));
            out.push_back(LSElement(LS_ROLL, phi1));
            out.push_back(LSElement(LS_A, s*r1, w*pow(q,e)));
            out.push_back(LSElement(LS_RIGHT_BRACKET));
            out.push_back(LSElement(LS_LEFT_BRACKET));
            out.push_back(LSElement(LS_TURN, alpha2));
            out.push_back(LSElement(LS_ROLL, phi2));
            out.push_back(LSElement(LS_A, s*r2, w*pow(1-q,e)));
            out.push_back(LSElement(LS_RIGHT_BRACKET));
            return true;
        }
    }
    else out.push_back(elem);

    return false;
}

// Added to work with QQueue
bool Rule::apply(const LSElement elem, QQueue<LSElement>& out)
{
    if(elem.symbol == LS_A)
    {
        float s = elem.datum1;
        float w = elem.datum2;

        if(s>=smin)
        {
            out.push_back(LSElement(LS_WIDTH, w));
            out.push_back(LSElement(LS_DRAW, s));
            out.push_back(LSElement(LS_LEFT_BRACKET));
            out.push_back(LSElement(LS_TURN, alpha1));
            out.push_back(LSElement(LS_ROLL, phi1));
            out.push_back(LSElement(LS_A, s*r1, w*pow(q,e)));
            out.push_back(LSElement(LS_RIGHT_BRACKET));
            out.push_back(LSElement(LS_LEFT_BRACKET));
            out.push_back(LSElement(LS_TURN, alpha2));
            out.push_back(LSElement(LS_ROLL, phi2));
            out.push_back(LSElement(LS_A, s*r2, w*pow(1-q,e)));
            out.push_back(LSElement(LS_RIGHT_BRACKET));
            return true;
        }
    }
    else out.push_back(elem);

    return false;
}

void LSElement::print(std::ostream &os)
{
    os << static_cast<char>(symbol);
    if(data>0)
    {
        os << "(" << datum1;
        if(data>1)
            os << "," << datum2;
        os << ")";
    }
}
