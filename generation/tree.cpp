#include "tree.h"

void Tree::regenSymbolString()
{
    symbolString.clear();

    symbolString.push_back(LSElement(LS_A, baseheight, basewidth));

    for(int iter=0;iter<rules.size();++iter)
    {
        QQueue<LSElement> out_str;
        for(int e=0;e<symbolString.size(); ++e)
        {
            rules[iter].apply(symbolString.at(e), out_str);
        }
        symbolString = out_str;
    }

    if (symbolString.size() == 1)
    {
        symbolString[0] = LSElement(LS_WIDTH, basewidth);
        symbolString.push_back(LSElement(LS_DRAW, baseheight));
    };
}

void Tree::regenGeometry()
{
    triangles.clear();
    normals.clear();

    interpret(symbolString, basewidth, triangles, normals);
    triStripLength = (GLuint)triangles.size();
}

void Tree::regenAbstract()
{
    abstractTree.clear();
    Turtle turtle(basewidth);
    Branch branch = {identity_Mat4x4f()};
    for(int i=0;i<symbolString.size(); ++i)
    {
        LSElement elem = symbolString[i];
        switch(elem.symbol)
        {
        case LS_DRAW:
            branch.instanceMatrix = turtle.get_transform();
            branch.instanceMatrix = branch.instanceMatrix * scaling_Mat4x4f(0.01*Vec3f(2*turtle.get_width(), 2*turtle.get_width(), elem.datum1));
            abstractTree.push_back(branch);
            turtle.move(elem.datum1);
            break;
        case LS_WIDTH:
            turtle.set_width(elem.datum1);
            break;
        case LS_LEFT_BRACKET:
            turtle.push();
            break;
        case LS_TURN:
            turtle.turn(elem.datum1);
            break;
        case LS_RIGHT_BRACKET:
            turtle.pop();
            break;
        case LS_ROLL:
            turtle.roll(elem.datum1);
            break;
            // Implement the other cases
        }
    }
}

void Tree::printSymbolStream()
{
    int lenSymStr = symbolString.size();
    LSElement tmp = LSElement(LS_TURN, 1);
    for (int i = 0; i < lenSymStr; i++)
    {
        tmp = symbolString.at(i);
        tmp.print(std::cout);
    }
    std::cout << std::endl;
}
