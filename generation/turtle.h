#ifndef TURTLE_H
#define TURTLE_H

#include <iostream>
#include <fstream>
#include <vector>

#include <QQueue>
#include <QStack>

#include <CGLA/Vec4f.h>
#include <CGLA/Mat4x4f.h>

#include "../flags.h"

using namespace CGLA;

const double pi = 3.141592653589;

// Symbols used for L-Systems
enum LSSymbol { LS_TURN = '+', LS_ROLL = '/', LS_WIDTH = '!', LS_A = 'A',
                LS_LEFT_BRACKET = '[', LS_RIGHT_BRACKET = ']', LS_DRAW = 'F'};

// LSElement is an LSSymbol and associated parameters.
struct LSElement
{
    LSSymbol symbol;
    double datum1, datum2;
    int data;

    LSElement(LSSymbol _symbol): symbol(_symbol), data(0) {}
    LSElement(LSSymbol _symbol, double _datum1): symbol(_symbol), datum1(_datum1), data(1) {}
    LSElement(LSSymbol _symbol, double _datum1, double _datum2): symbol(_symbol), datum1(_datum1), datum2(_datum2), data(2) {}

    void print(std::ostream& os);
};

class Rule
{
    float alpha1, alpha2, phi1, phi2, r1, r2, q, e, smin;

public:
    Rule(float _alpha1, float _alpha2, float _phi1, float _phi2,
         float _r1, float _r2, float _q, float _e, float _smin):
        alpha1(_alpha1), alpha2(_alpha2), phi1(_phi1), phi2(_phi2),
        r1(_r1), r2(_r2), q(_q), e(_e), smin(_smin) {}

    bool apply(const LSElement elem, std::vector<LSElement>& out);

    bool apply(const LSElement elem, QQueue<LSElement>& out);
};

// The state used for turtle graphics
struct TurtleState
{
    float w;
    Mat4x4f M;
    TurtleState(float w0, const Mat4x4f& M0): w(w0), M(M0) {}
};

// The turtle. Contains state and functions for crawling in 3D.
class Turtle
{
    TurtleState turtle_state;
    QList<TurtleState> tss;

public:
    Turtle(float w0): turtle_state(w0, identity_Mat4x4f()) {}
    void turn(float angle) {turtle_state.M *= rotation_Mat4x4f(YAXIS, angle/180*pi);}
    void roll(float angle) {turtle_state.M *= rotation_Mat4x4f(ZAXIS, angle/180*pi);}
    void move(float dist) {turtle_state.M *= translation_Mat4x4f(Vec3f(0,0,dist));}
    void push() {tss.push_back(turtle_state);}
    void pop() {turtle_state = tss.back(); tss.pop_back();}
    void set_width(float w) {turtle_state.w = w;}

    float get_width() const {return turtle_state.w;}
    const Mat4x4f& get_transform() const {return turtle_state.M;}
};

void interpret(QQueue<LSElement> str, float w0,
               QQueue<CGLA::Vec3f>& triangles, QQueue<CGLA::Vec3f>& normals);

#endif // TURTLE_H
