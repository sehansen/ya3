#include "TreeData.h"


TreeData::TreeData(const Vec3f &initPos)
    : basewidth(20),
      baseheight(100),
      triStripLength(CONESIZE*2),
      position(initPos)
    {
        symbolString.push_back(LSElement(LS_A, baseheight, basewidth));
        regenSymbolString();
        regenGeometry();

        Reset();
    }


void TreeData::gen(Branch *node)
{
    BranchPtr child = BranchPtr(new Branch());
    tree.push_back(child);
    child->m_turn = -0.1 - 0.0005f * (rand() % 1000);
    node->m_children.push_back(child.get());
    child->m_parent = node;
    child->m_side = YA3Side_Left;
    child->m_expectedLength = (1.5f + randf()) * (9 - m_iterations) / 8.0f;

    leaves.append(child);

    //Right Branch
    child = BranchPtr(new Branch());
    child->m_turn = 0.1 + 0.0005f * (rand() % 1000);
    tree.push_back(child);
    node->m_children.push_back(child.get());
    child->m_parent = node;
    child->m_side = YA3Side_Right;
    child->m_expectedLength = (1.5f + randf()) *  (9 -  m_iterations) / 8.0f;

    leaves.append(child);
}

void TreeData::regenSymbolString()
{
    symbolString.clear();

    symbolString.push_back(LSElement(LS_A, baseheight, basewidth));

    for(int iter=0;iter<rules.size();++iter)
    {
        QQueue<LSElement> out_str;
        for(int e=0;e<symbolString.size(); ++e)
        {
            rules[iter].apply(symbolString.at(e), out_str);
        }
        symbolString = out_str;
    }

    if (symbolString.size() == 1)
    {
        symbolString[0] = LSElement(LS_WIDTH, basewidth);
        symbolString.push_back(LSElement(LS_DRAW, baseheight));
    };
}

void TreeData::regenGeometry()
{
    triangles.clear();
    normals.clear();

    interpret(symbolString, basewidth, triangles, normals);
    triStripLength = (GLuint)triangles.size();
}

/*
void TreeData::regenAbstract()
{
    abstractTree.clear();
    Turtle turtle(basewidth);
    Branch branch = {identity_Mat4x4f()};
    for(int i=0;i<symbolString.size(); ++i)
    {
        LSElement elem = symbolString[i];
        switch(elem.symbol)
        {
        case LS_DRAW:
            branch.instanceMatrix = turtle.get_transform();
            branch.instanceMatrix = branch.instanceMatrix * scaling_Mat4x4f(0.01*Vec3f(2*turtle.get_width(), 2*turtle.get_width(), elem.datum1));
            abstractTree.push_back(branch);
            turtle.move(elem.datum1);
            break;
        case LS_WIDTH:
            turtle.set_width(elem.datum1);
            break;
        case LS_LEFT_BRACKET:
            turtle.push();
            break;
        case LS_TURN:
            turtle.turn(elem.datum1);
            break;
        case LS_RIGHT_BRACKET:
            turtle.pop();
            break;
        case LS_ROLL:
            turtle.roll(elem.datum1);
            break;
            // Implement the other cases
        }
    }
}*/

int TreeData::getTriStripLength()
{
    return triStripLength;
}

QQueue<CGLA::Vec3f> TreeData::getTriangles()
{
    return triangles;
}

QQueue<CGLA::Vec3f> TreeData::getNormals()
{
    return normals;
}

Vec3f TreeData::getPosition()
{
    return position;
}

void TreeData::setPosition(Vec3f newPosition)
{
    position = newPosition;
}





void TreeData::Update(float timeStep)
{
    m_totalTime += timeStep;
    m_time -= timeStep;

    if(m_time < 0 && m_iterations < 8   )
    {
        m_time = m_timePerBranch;

        auto oldLeaves = leaves;
        leaves.clear();

        QListIterator<std::shared_ptr<Branch>> it(oldLeaves);
        while (it.hasNext())
        {
            auto n = it.next();
            gen(n.get());
        }

        qDebug() << "Branches: "<< tree.size();

        m_iterations += 1;
    }

    float t = (m_timePerBranch - m_time) / m_timePerBranch;
    if(t < 0.0f)
        t = 0.0f;
    else if(t > 1)
        t = 1;

    float gscale = m_totalTime / (m_timePerBranch * 8.0f * 2.0f) + 0.5f;
    if(gscale < 0.5f)
        gscale = 0.5f;
    else if(gscale > 1.0f)
        gscale = 1.0f;

    //qDebug() << gscale;
    /*QListIterator<std::shared_ptr<Branch>> it(leaves);
    while (it.hasNext())
    {
        BranchPtr b = it.next();
        b->m_length = (b->m_expectedLength * b->m_expectedLengthFactor) * t;
    }*/
    QListIterator<std::shared_ptr<Branch>> it(tree);
    while (it.hasNext())
    {
        BranchPtr b = it.next();
        if(b->m_children.size() == 0)
        {
            b->m_length = gscale * (b->m_expectedLength * b->m_expectedLengthFactor) * t;
        }
        else
        {
            b->m_length += (gscale * b->m_expectedLength * b->m_expectedLengthFactor - b->m_length) * timeStep;
        }
    }
}


void TreeData::GenerateMatrices(Branch* node, const CGLA::Mat4x4f& rot, const CGLA::Vec3f& point, QQueue<CGLA::Mat4x4f>& out, float radius)
{
    CGLA::Mat4x4f localRot = CGLA::rotation_Mat4x4f(CGLA::ZAXIS, node->m_roll) *
            CGLA::rotation_Mat4x4f(CGLA::XAXIS, node->m_turn+node->m_turnOffset);

    CGLA::Mat4x4f nextrot = rot * localRot;
    CGLA::Vec3f nextpoint = point + nextrot.mul_3D_point(Vec3f(0, 0, 50 * node->m_length ));

    node->m_worldOriginPoint = point;

    out.push_back(
                CGLA::translation_Mat4x4f(point) *
                nextrot *
                CGLA::scaling_Mat4x4f(Vec3f(radius, radius, node->m_length * 0.5f ))
                );

    QVectorIterator<Branch*> it(node->m_children);
    while (it.hasNext())
    {
        GenerateMatrices(it.next(), nextrot, nextpoint, out, radius * 0.65f);
    }
}

QQueue<CGLA::Mat4x4f> TreeData::getInstanceMatrices()
{
    QQueue<CGLA::Mat4x4f> instanceMatrices;

    if(tree.length() <= 0)
        return instanceMatrices;

    CGLA::Mat4x4f rotation;
    rotation.identity();

    float radius = 1.2f * m_totalTime / m_timePerBranch;
    if(radius > 14.0f)
        radius = 14.0f;

    GenerateMatrices(tree[0].get(), rotation, 50 * position, instanceMatrices, radius * 0.12f);

    return instanceMatrices;
}


void TreeData::TiltInteraction(CGLA::Vec3f viewdir, float tilt, float falloff)
{
    doTilt(tree[0].get(), viewdir, tilt, falloff);
}

void TreeData::doTilt(Branch* current, CGLA::Vec3f viewdir, float tilt, float falloff)
{
    if(std::fabs(tilt)< 0.01f)
    {
        return;
    }

    //offset
    current->m_turnOffset = tilt;
    tilt *= falloff;

    //recurse
    QVectorIterator<Branch*> it(current->m_children);
    while (it.hasNext())
    {
        Branch* child = it.next();
        doTilt(child, viewdir, tilt, falloff);
    }

}

void TreeData::LengthInteraction(const CGLA::Vec3f& viewdir, float length, float weight, float falloff, YA3Side side)
{
    //qDebug() << spread;

    QListIterator<BranchPtr> it(leaves);
    while (it.hasNext())
    {
        Branch* child = it.next().get();
        doLength(child, viewdir, length, weight, falloff, side);
    }
}

void TreeData::doLength(Branch* current, const CGLA::Vec3f viewdir, float length, float weight, float falloff, YA3Side side)
{
    if(current->m_side == side)
    {
        float v = length * weight;

        //scale
        v *= 3.0f;

        //clamp
        if(v < -1.0f)
            v = -1.0f;
        else if(v > 1.0f)
            v = 1.0f;

        //map to ratio
        //0.5 - 1.5, length:0 -> v:1
        v = 1 + v * 0.5f;

        current->m_expectedLengthFactor = v;

        weight *= falloff;
        if(weight > 0.01f)
        {
            doLength(current->m_parent, viewdir, length, weight, falloff, side);
        }
    }
}


void TreeData::SpreadInteraction(const CGLA::Vec3f& viewdir, float spread, float weight, float falloff, YA3Side side)
{
    //qDebug() << spread;

    QListIterator<BranchPtr> it(leaves);
    while (it.hasNext())
    {
        Branch* child = it.next().get();
        doSpread(child, viewdir, spread, weight, falloff, side);
    }
}

void TreeData::doSpread(Branch* current, const CGLA::Vec3f viewdir, float spread, float weight, float falloff, YA3Side side)
{
    //if(current->m_parent && current->m_parent->m_side != YA3Side_NoSide)
    if(current->m_side == side)
    {

        current->m_turn += (spread  * (current->m_side == YA3Side_Left ? -1 : 1) - current->m_turn) * weight * weight  * weight * weight;
        //current->m_turn += (spread  * ((current->m_worldOriginPoint/50 - position)[2] < 0 ? -1 : 1) - current->m_turn) * weight;

//            qDebug() << (current->m_worldOriginPoint/50 - position)[0];

        //qDebug() << spread;
        weight *= falloff;
     //   spread += (current->m_turn - spread* (current->m_side == YA3Side_Left ? -1 : 1)) * (1.0f - falloff * falloff);

//          weight *= falloff;
        if(weight > 0.01f)
        {
            doSpread(current->m_parent, viewdir, spread, weight, falloff, side);
        }
    }
}

void TreeData::Reset()
{
    tree.clear();
    leaves.clear();

    BranchPtr root = BranchPtr(new Branch());
    tree.push_back(root);
    leaves.push_back(root);

    root->m_roll = root->m_turn = 0;
    m_timePerBranch = 6.0f;
    m_time = m_timePerBranch;
    m_iterations = 1;
    m_totalTime = 0.0f;
}












