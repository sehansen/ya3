#ifndef TREE_DATA_H
#define TREE_DATA_H

#include <QDebug>
#include <QtGlobal>
#include <QList>
#include <QQueue>
#include <memory>

#include <CGLA/Vec4f.h>
#include <CGLA/Mat4x4f.h>
#include <GLGraphics/GLHeader.h>

#include "../interaction/skeletonutils.h"
#include "../flags.h"
#include "turtle.h"

//TODO:: move randf to math namespace
static float randf()
{
    return (float)rand() / (float)RAND_MAX;
}

struct Branch
{
    QVector<Branch*> m_children;
    Branch* m_parent;

    YA3Side m_side;
    float m_length;
    float m_lengthScale;
    float m_expectedLength;
    float m_expectedLengthFactor;
    float m_turn;
    float m_roll;
    float m_startRadius;
    float m_endRadius;
    float m_turnOffset;
    CGLA::Vec3f m_worldOriginPoint;

    Branch()
    {
        m_side = YA3Side_NoSide;
        m_parent = nullptr;
        m_length = 2.0f;
        m_expectedLength = 2.5f;
        m_expectedLengthFactor = 1.0f;
        m_turn = 0.001f * (float)(rand()%1000);
        m_roll = 1.6f * (0.5f - randf());
        m_startRadius = 1.0f;
        m_endRadius = 1.0f;
        m_turnOffset = 0.0f;
        m_lengthScale = 0.5f;
    }
};


typedef std::shared_ptr<Branch> BranchPtr;


class TreeData
{
    float basewidth, baseheight;

    QQueue<Rule> rules;
    QQueue<LSElement> symbolString;

    QList<Branch> abstractTree;
    QQueue<CGLA::Mat4x4f> instanceMatrices;

    QQueue<CGLA::Vec3f> triangles, normals;
    int triStripLength;

    Vec3f position;

    QList<std::shared_ptr<Branch>> tree;
    QList<std::shared_ptr<Branch>> leaves;

    float m_timePerBranch;
    float m_time;
    int m_iterations;
    float m_totalTime;

public:


    TreeData(const Vec3f& initPos = Vec3f(0,0,3.21693));

    void gen(Branch* node);
    void regenSymbolString();
    void regenGeometry();

    int getTriStripLength();
    QQueue<CGLA::Vec3f> getTriangles();
    QQueue<CGLA::Vec3f> getNormals();
    Vec3f getPosition();
    void setPosition(Vec3f newPosition);

    void Update(float timeStep);
    void GenerateMatrices(Branch* node, const CGLA::Mat4x4f& rot, const CGLA::Vec3f& point, QQueue<CGLA::Mat4x4f>& out, float radius);
    QQueue<CGLA::Mat4x4f> getInstanceMatrices();

    void TiltInteraction(CGLA::Vec3f viewdir, float tilt, float falloff);
    void doTilt(Branch* current, CGLA::Vec3f viewdir, float tilt, float falloff);
    void LengthInteraction(const CGLA::Vec3f& viewdir, float length, float weight, float falloff, YA3Side side);
    void doLength(Branch* current, const CGLA::Vec3f viewdir, float length, float weight, float falloff, YA3Side side);
    void SpreadInteraction(const CGLA::Vec3f& viewdir, float spread, float weight, float falloff, YA3Side side);
    void doSpread(Branch* current, const CGLA::Vec3f viewdir, float spread, float weight, float falloff, YA3Side side);

    void Reset();
};

#endif // TREE_DATA_H
