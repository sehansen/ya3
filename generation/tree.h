#ifndef OLDTREE_H
#define OLDTREE_H

#include <QDebug>
#include <QtGlobal>
#include <QList>
#include <QQueue>

#include <CGLA/Vec4f.h>
#include <CGLA/Mat4x4f.h>
#include <GLGraphics/GLHeader.h>

#include "../TerrainScene/tests.h"
#include "turtle.h"

struct Branch
{
    Mat4x4f instanceMatrix;
};

class Tree
{
    float basewidth, baseheight;

    QQueue<Rule> rules;
    QQueue<LSElement> symbolString;

    QList<Branch> abstractTree;
    QQueue<CGLA::Mat4x4f> instanceMatrices;

    QQueue<CGLA::Vec3f> triangles, normals;
    int triStripLength;

    Vec3f position;

public:
    Tree(const Vec3f& initPos = Vec3f(0,0,3.21693)) : basewidth(20), baseheight(100), rules(), symbolString(), triStripLength(CONESIZE*2), position(initPos)
    {
        symbolString.push_back(LSElement(LS_A, baseheight, basewidth));
        regenSymbolString();
        regenGeometry();
        regenAbstract();
    }

    void pushRule(Rule ex)
    {
        rules.push_back(ex);
    }

    void popRule()
    {
        rules.pop_back();
    }

    void regenSymbolString();

    void regenGeometry();

    void regenAbstract();

    void printSymbolStream();

    int getTriStripLength()
    {
        return triStripLength;
    }

    QQueue<CGLA::Vec3f> getTriangles()
    {
        return triangles;
    }

    QQueue<CGLA::Vec3f> getNormals()
    {
        return normals;
    }

    QQueue<CGLA::Mat4x4f> getInstanceMatrices()
    {
        QQueue<CGLA::Mat4x4f> instanceMatrices;
        QListIterator<Branch> i(abstractTree);
        while (i.hasNext())
            instanceMatrices.push_back(translation_Mat4x4f(50*position) * i.next().instanceMatrix);
        return instanceMatrices;
    }

    int getRulesSize()
    {
        return rules.size();
    }

    void clearRules()
    {
        rules.clear();
    }

    Vec3f getPosition()
    {
        return position;
    }

    void setPosition(Vec3f newPosition)
    {
        position = newPosition;
    }
};

#endif // OLDTREE_H
