#ifndef DEBUGWINDOW_H
#define DEBUGWINDOW_H

#include <QMainWindow>

#include "interaction/inputhelper.h"

namespace Ui {
class DebugWindow;
}

class DebugWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DebugWindow(QWidget *parent = 0);
    ~DebugWindow();

    void SetInputHelper(InputHelper* inputHelper){m_inputHelper = inputHelper;}

protected:
    void paintEvent(QPaintEvent *event);


private:
    InputHelper* m_inputHelper;
    Ui::DebugWindow *ui;
    QTimer* timer;
};

#endif // DEBUGWINDOW_H
