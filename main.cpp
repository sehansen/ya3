#include "controlwindow.h"
#include "debugwindow.h"

#include <iostream>

#include <QApplication>
#include <QTimer>
#include <QTime>
#include <QDebug>
#include <QMouseEvent>

#include <GLGraphics/GLHeader.h>
#include <GLGraphics/ThreeDObject.h>
#include <GLGraphics/User.h>
#include <GLGraphics/ShaderProgram.h>
#include <CGLA/Mat4x4f.h>
#include <CGLA/Vec3f.h>
#include <CGLA/Vec2f.h>
#include <NuiApi.h>

#include "scene/Scene.h"
#include "soundsystem.h"

int main(int argc, char *argv[])
{

    qDebug() << "\n\n >>>>>>>>>>>>> SETTING UP <<<<<<<<<<<<<<<<<<<\n";
    //qDebug() << argv[0] << " - " << argv[1] << endl;

    const int WINDOW_WIDTH = 1136;
    const int WINDOW_HEIGHT = 640;

    QApplication a(argc, argv);

    // Control Window
    ControlWindow wc;
    wc.show();

    // Sound
#ifndef MUTE_SOUND
    SoundSystem ss;
#endif

    // Debug Window
    DebugWindow debugWindow;
    debugWindow.show();

    // Run Application
    QGLFormat glFormat;
    glFormat.setVersion(4, 5);
    glFormat.setProfile(QGLFormat::CoreProfile); // Requires >=Qt-4.8.0

    Scene scene(glFormat);
    scene.resize(WINDOW_WIDTH, WINDOW_HEIGHT);
    scene.setFocusPolicy(Qt::ClickFocus);
    scene.show();

    debugWindow.SetInputHelper(&scene.getInputHelper());


    return a.exec();
}
