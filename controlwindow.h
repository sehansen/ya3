#ifndef CONTROLWINDOW_H
#define CONTROLWINDOW_H

//#include <GLGraphics/GLHeader.h>
//#include <GLGraphics/ThreeDObject.h>
//#include <GLGraphics/ShaderProgram.h>

#include <QMainWindow>

namespace Ui {
class ControlWindow;
}

class ControlWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ControlWindow(QWidget *parent = 0);
    ~ControlWindow();

private:
    Ui::ControlWindow *ui;
};

#endif // CONTROLWINDOW_H
