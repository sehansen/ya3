#ifndef KEYBOARDHELPER_H
#define KEYBOARDHELPER_H

#include <map>
#include <vector>
#include <memory>
#include <string>

#include "inputhelper.h"

class KeyboardHelper : public InputHelper
{
public:
    KeyboardHelper();

    virtual bool Init();
    virtual void Update(float timeStep);

    void FindCurrentPlayer();
    
private:

    std::map<std::string, std::unique_ptr<std::vector<CGLA::Vec3f>>> m_poses;
    std::vector<CGLA::Vec3f> m_outSkeleton;
    std::vector<CGLA::Vec3f> m_targetSkeleton;
    float m_time2Pi;
    int m_target;
};

#endif // KEYBOARDHELPER_H
