#include "skeletonutils.h"

SkeletonUtils::SkeletonUtils() {}

void SkeletonUtils::LoadPose(std::stringstream &istream, std::vector<CGLA::Vec3f> &joints, std::string& name)
{
    joints.resize(YA3_JOINT_COUNT);
    istream >> name;

    for(auto& vec : joints)
    {
        istream >> vec[0] >> vec[1] >> vec[2];
    }
}


void SkeletonUtils::LerpSkeleton(const SkeletonData& a, const SkeletonData& b, float t, SkeletonData& out)
{
    for(size_t i = 0; i < YA3_JOINT_COUNT; ++i)
    {
        out[i] = t * b[i] + (1.0f - t) * a[i];
    }
}

void SkeletonUtils::LerpSkeleton(std::vector<CGLA::Vec3f>& source, float factor, const std::vector<CGLA::Vec3f>& target)
{
    for(size_t i = 0; i < YA3_JOINT_COUNT; ++i)
    {
        source[i] += (target[i] - source[i]) * factor;
    }
}
