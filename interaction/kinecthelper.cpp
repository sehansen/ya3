#include "kinecthelper.h"

#include <vector>
#include <QDebug>

#include "../flags.h"
#include "skeletonutils.h"

#define degreesToRadians(angleDegrees) (angleDegrees * M_PI / 180.0)
#define radiansToDegrees(angleRadians) (angleRadians * 180.0 / M_PI)

NUI_SKELETON_POSITION_INDEX g_jointTable[] =
{
    NUI_SKELETON_POSITION_HAND_LEFT,        //YA3_LHand
    NUI_SKELETON_POSITION_ELBOW_LEFT,       //YA3_LElbow
    NUI_SKELETON_POSITION_SHOULDER_LEFT,    //YA3_LShoulder
    NUI_SKELETON_POSITION_SHOULDER_CENTER,  //YA3_ShoulderCenter
    NUI_SKELETON_POSITION_HIP_CENTER,       //YA3_HipCenter
    NUI_SKELETON_POSITION_HAND_RIGHT,       //YA3_RHand
    NUI_SKELETON_POSITION_ELBOW_RIGHT,      //YA3_RElbow
    NUI_SKELETON_POSITION_SHOULDER_RIGHT    //YA3_RShoulder
};

KinectHelper::KinectHelper()
    : m_sensor(0), m_skeletonEventListener(INVALID_HANDLE_VALUE), m_findCurrentPlayerInterval(1000)
{
    qDebug() << "[KINECTHELPER] Constructing kinecthelper" << endl;

    m_defaultPoseIntermediate.resize(YA3_JOINT_COUNT);

}

bool KinectHelper::Init()
{
    qDebug() << "[KINECTHELPER] Initializing...   ";
    int sensorCount = 0;
    HRESULT hr = NuiGetSensorCount(&sensorCount);

    INuiSensor* sensor = 0;
    for(int i = 0; i < sensorCount; ++i)
    {
        hr = NuiCreateSensorByIndex(i, &sensor);
        if(FAILED(hr))
        {
            continue;
        }

        //sensor ok?
        hr = sensor->NuiStatus();
        if(hr == S_OK)
        {
           m_sensor = sensor;
           break;
        }

        sensor->Release();
    }

    if(NULL != m_sensor)
    {
        //initialize sensor
        DWORD sensorflags = NUI_INITIALIZE_FLAG_USES_SKELETON;
        hr = m_sensor->NuiInitialize(sensorflags);
        if(SUCCEEDED(hr))
        {
            DWORD skeletonFlags = NUI_SKELETON_TRACKING_FLAG_ENABLE_IN_NEAR_RANGE;
            m_skeletonEventListener = CreateEvent(NULL, TRUE, FALSE, NULL);
            hr = m_sensor->NuiSkeletonTrackingEnable(m_skeletonEventListener, skeletonFlags);
            if(FAILED(hr))
            {
                qDebug() << "[KINECTHELPER] FAILED. (Could not register skeleton event listener)" << endl;
                m_sensor->Release();
                return false;
            }
        }
        else
        {
            qDebug() << "[KINECTHELPER] FAILED. (failed to initialize sensor)" << endl;
            m_sensor->Release();
            return false;
        }
    }
    else
    {
        qDebug() << "[KINECTHELPER] FAILED. (No Sensor Found)!" << endl;
        return false;
    }


    // SUCESS! Find current player and initialization is done
    qDebug() << "[KINECTHELPER] SUCCESS!" << endl;

    //FindCurrentPlayerTimer = new QTimer(this);
    //connect(FindCurrentPlayerTimer, SIGNAL(timeout()), this, SLOT(FindCurrentPlayer()));
    //FindCurrentPlayerTimer->start(m_findCurrentPlayerInterval);
    //FindCurrentPlayer(); //call it once in the beginning too

    //NuiCameraElevationSetAngle(0);

    m_initialized = true;
    return true;
}

void KinectHelper::Update(float timeStep)
{
    InputHelper::Update(timeStep);
    NUI_SKELETON_FRAME sframe = {0};

    //lerp towards default if no player
    if(m_currentPlayer == -1)
    {
        SkeletonUtils::LerpSkeleton(m_joints, m_defaultSkeleton, timeStep*1.0f, m_defaultPoseIntermediate);
        for(int j = 0; j < YA3_JOINT_COUNT; ++j)
        {
            //qDebug () << "to default" << endl;
            UpdateJoint((YA3Joint)j, m_defaultPoseIntermediate[j]);
        }
    }


    //sensor & event ok?
    if(m_sensor == NULL)
    {
        qDebug() << "[KINECTHELPER] Sensor is NULL!"<< endl;
        return;
    }

    //disconnected?
    HRESULT nuistatus = m_sensor->NuiStatus();
    if (nuistatus == E_NUI_NOTCONNECTED)
    {
        if(m_initialized)
        {
            qDebug() << "[KINECTHELPER] Disconnected! :( " << endl;
        }
        m_initialized = false;
        m_currentPlayer = -1;
        return;
    }

    //kinect event?
    if(WAIT_OBJECT_0 != WaitForSingleObject(m_skeletonEventListener, 0))
    {
        return;
    }

    //new skeleton frame&
    HRESULT hr = m_sensor->NuiSkeletonGetNextFrame(0, &sframe);
    if(FAILED(hr))
    {
        qDebug() << hr << endl;
        return;
    }

    //Update Current Player
    FindCurrentPlayer(sframe);
    if(m_currentPlayer == -1)
    {
        return;
    }

    //Track Skeleton of player
    NUI_SKELETON_TRACKING_STATE trackingstate = sframe.SkeletonData[m_currentPlayer].eTrackingState;
    if(NUI_SKELETON_TRACKED == trackingstate)
    {
#ifdef DEBUG_INPUT_BOUNDSCLIPPING
        if(sframe.SkeletonData[m_currentPlayer].dwQualityFlags & NUI_SKELETON_QUALITY_CLIPPED_BOTTOM)
           qDebug() << "[KINECTHELPER] CLIPPED_BOTTOM of skeleton " << m_currentPlayer << endl;
        if(sframe.SkeletonData[m_currentPlayer].dwQualityFlags & NUI_SKELETON_QUALITY_CLIPPED_TOP)
           qDebug() << "[KINECTHELPER] CLIPPED_TOP of skeleton " << m_currentPlayer << endl;
        if(sframe.SkeletonData[m_currentPlayer].dwQualityFlags & NUI_SKELETON_QUALITY_CLIPPED_LEFT)
           qDebug() << "[KINECTHELPER] CLIPPED_LEFT of skeleton " << m_currentPlayer << endl;
        if(sframe.SkeletonData[m_currentPlayer].dwQualityFlags & NUI_SKELETON_QUALITY_CLIPPED_RIGHT)
           qDebug() << "[KINECTHELPER] CLIPPED_RIGHT of skeleton " << m_currentPlayer << endl;
#endif

        for(int j = 0; j < YA3_JOINT_COUNT; ++j)
        {
            NUI_SKELETON_POSITION_INDEX joint = g_jointTable[j];
            NUI_SKELETON_POSITION_TRACKING_STATE jointState = sframe.SkeletonData[m_currentPlayer].eSkeletonPositionTrackingState[joint];
            if(jointState !=NUI_SKELETON_POSITION_NOT_TRACKED)
            {
                const Vector4& pos = sframe.SkeletonData[m_currentPlayer].SkeletonPositions[joint];
                UpdateJoint((YA3Joint)j, CGLA::Vec3f(pos.x, pos.y, pos.z));
            }
        }
    }
}

void KinectHelper::FindCurrentPlayer(NUI_SKELETON_FRAME& sframe)
{
    int closestPlayer =-1;
    float bestdist = 1e7;
    CGLA::Vec3f playerpos;
    for(int i = 0; i < NUI_SKELETON_COUNT; ++i) //TODO: Player Count
    {
        NUI_SKELETON_TRACKING_STATE trackingstate = sframe.SkeletonData[i].eTrackingState;
        if(NUI_SKELETON_TRACKED == trackingstate)
        {
            //how close?
            NUI_SKELETON_POSITION_TRACKING_STATE hipstate = sframe.SkeletonData[i].eSkeletonPositionTrackingState[NUI_SKELETON_POSITION_HIP_CENTER];
            if(hipstate != NUI_SKELETON_POSITION_NOT_TRACKED)
            {
                float dist = sframe.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].z;
                if(dist < bestdist)
                {
                    closestPlayer = i;
                    bestdist = dist;
                    playerpos[0] = sframe.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].x;
                    playerpos[1] = sframe.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].y;
                    playerpos[2] = sframe.SkeletonData[i].SkeletonPositions[NUI_SKELETON_POSITION_HIP_CENTER].z;
                }
            }
        }
    }

    if(closestPlayer != -1)
    {
        if(m_currentPlayer != closestPlayer)
        {
            qDebug() << "[KINECTHELPER] Current player -->" << closestPlayer << endl;
            m_currentPlayer = closestPlayer;

            // aim towards new player
#ifdef SET_ELEVATION_ANGLE_ON_NEW_PLAYER
            CGLA::Vec3f tocam = -playerpos;
            tocam.normalize();
            float rad = (-std::acosf(CGLA::dot(tocam, CGLA::Vec3f(0, 0, -1))));
            NuiCameraElevationSetAngle(radiansToDegrees(rad));
#endif
        }
    }
    else
    {
        if(m_currentPlayer != -1)
        {
            qDebug() << "[KINECTHELPER] no player." << endl;
            m_currentPlayer = -1;
        }
    }

}

KinectHelper::~KinectHelper()
{

}
