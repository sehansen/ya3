#include "inputhelper.h"

#include <algorithm>
#include <sstream>

#include <QDebug>

#include "filter/inputfilter.h"
#include "filter/nofilter.h"
#include "filter/simplesmoothfilter.h"

/* Helper Functions & Typedefs */

float point_distance_xy(CGLA::Vec3f a, CGLA::Vec3f b)
{
    a[2] = 0;
    b[2] = 0;
    return CGLA::length(a-b);
}

float inverseSqrt(float x)
{
    float xhalf = 0.5f*x;
    int i = *(int*)&x;          // get bits for floating value
    i = 0x5f375a86- (i>>1);     // gives initial guess y0
    x = *(float*)&i;            // convert bits back to float
    x = x*(1.5f-xhalf*x*x);     // Newton step, repeating increases accuracy
    return x;
}

float point_segment_distance(CGLA::Vec3f p, CGLA::Vec3f a, CGLA::Vec3f b)
{
    CGLA::Vec3f ab = b - a;
    CGLA::Vec3f ap = p - a;
    float ab_sqlen = CGLA::sqr_length(ab);
    float t = CGLA::dot(ab, ap);
    if(t < 0)
        return point_distance_xy(p, a);
    if(t > ab_sqlen)
        return point_distance_xy(p, b);

    float ab_len_inv = inverseSqrt(ab_sqlen); //1.f/std::sqrtf(ab_sqlen);
    t  *= ab_len_inv;
    ab *= ab_len_inv;

    return CGLA::length(ap - t*ab);
}



/* InputHelper Implementation */

InputHelper::InputHelper():
    m_inputFilter(nullptr), m_initialized(false), m_currentPlayer(-1)
{
    m_joints.resize(YA3_JOINT_COUNT);
    m_rawJoints.resize(YA3_JOINT_COUNT);

    //std::string defaultPoseData = "default 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0";
    std::string defaultPoseData = "default -0.49507 -0.191841 2.08651 -0.435837 0.0836634 2.26272 -0.377198 0.297074 2.27661 -0.184404 0.434872 2.24076 -0.181199 -0.00708832 2.1547 0.102583 -0.20407 2.00352 0.0527786 0.0787015 2.18445 -0.029793 0.322875 2.19924";
    std::stringstream istream(defaultPoseData);
    std::string poseName;
    SkeletonUtils::LoadPose(istream, m_defaultSkeleton, poseName);
    m_joints = m_defaultSkeleton;
    m_rawJoints = m_defaultSkeleton;

    //Filtering method. TODO: set filter by method
    //m_inputFilter = new NoFilter(m_joints);
    m_inputFilter = new SimpleSmoothFilter(m_joints, 10.f, 0.005f);
}

InputHelper::~InputHelper() {}


bool InputHelper::IsInitialized() const
{
    return m_initialized;
}

const CGLA::Vec3f& InputHelper::JointPosition(YA3Joint joint) const
{
    return m_joints[joint];
}


const CGLA::Vec3f& InputHelper::RawJointPosition(YA3Joint joint) const
{
    return m_rawJoints[joint];
}

void InputHelper::projectJoint(CGLA::Vec3f& store, YA3Joint joint) const
{
    store[0] = m_joints[joint][0];
    store[1] = m_joints[joint][1];
    store[2] = 0;
}


float InputHelper::HandToShoulderAngle(YA3Side side) const
{
    CGLA::Vec3f hand, shoulder, shouldercenter, hipcenter;
    projectJoint(hand, side == YA3Side_Left ? YA3Joint_LHand : YA3Joint_RHand);
    projectJoint(shoulder, side == YA3Side_Left ? YA3Joint_LShoulder : YA3Joint_RShoulder);
    projectJoint(shouldercenter, YA3Joint_ShoulderCenter);
    projectJoint(hipcenter, YA3Joint_HipCenter);

    CGLA::Vec3f shoulderToHand = hand - shoulder;
    CGLA::Vec3f torso = shouldercenter - hipcenter;
    shoulderToHand.normalize();
    torso.normalize();

    return std::acosf(CGLA::dot(-torso, shoulderToHand));
}

float InputHelper::HandToShoulderDistance(YA3Side side) const
{
    CGLA::Vec3f hand, shoulder;
    projectJoint(hand, side == YA3Side_Left ? YA3Joint_LHand : YA3Joint_RHand);
    projectJoint(shoulder, side == YA3Side_Left ? YA3Joint_LShoulder : YA3Joint_RShoulder);

    return CGLA::length(hand - shoulder);
}

float InputHelper::HandToShoulderDistance3D(YA3Side side) const
{
    return CGLA::length(
                m_joints[side == YA3Side_Left ? YA3Joint_LHand : YA3Joint_RHand] -
                m_joints[side == YA3Side_Left ? YA3Joint_LShoulder : YA3Joint_RShoulder]);
}

float InputHelper::ElbowToShoulderAngle(YA3Side side) const //TODO: elbowtoTorso instead
{
    CGLA::Vec3f elbow, shoulder, shouldercenter, hipcenter;
    projectJoint(elbow, side == YA3Side_Left ? YA3Joint_LElbow : YA3Joint_RElbow);
    projectJoint(shoulder, side == YA3Side_Left ? YA3Joint_LShoulder : YA3Joint_RShoulder);
    projectJoint(shouldercenter, YA3Joint_ShoulderCenter);
    projectJoint(hipcenter, YA3Joint_HipCenter);

    CGLA::Vec3f shoulderToElbow = elbow  - shoulder;
    CGLA::Vec3f torso = hipcenter - shouldercenter; //downward
    shoulderToElbow.normalize();
    torso.normalize();

    return std::acosf(CGLA::dot(torso, shoulderToElbow));
}

float InputHelper::ElbowToShoulderDistance(YA3Side side) const
{
    CGLA::Vec3f elbow, shoulder;
    projectJoint(elbow, side == YA3Side_Left ? YA3Joint_LElbow : YA3Joint_RElbow);
    projectJoint(shoulder, side == YA3Side_Left ? YA3Joint_LShoulder : YA3Joint_RShoulder);

    return CGLA::length(elbow - shoulder);
}

float InputHelper::ElbowToHandAngle(YA3Side side) const
{
    CGLA::Vec3f shoulder, elbow, hand;
    projectJoint(shoulder, side == YA3Side_Left ? YA3Joint_LShoulder : YA3Joint_RShoulder);
    projectJoint(elbow, side == YA3Side_Left ? YA3Joint_LElbow : YA3Joint_RElbow);
    projectJoint(hand, side == YA3Side_Left ? YA3Joint_LHand : YA3Joint_RHand);

    CGLA::Vec3f upperarm = shoulder - elbow;
    CGLA::Vec3f lowerarm = hand - elbow;

    //Here Be Dragons
    upperarm.normalize();
    lowerarm.normalize();

    return std::acosf(CGLA::dot(upperarm, lowerarm));
}

float InputHelper::ElbowToHandDistance(YA3Side side) const
{
    CGLA::Vec3f elbow, hand;
    projectJoint(elbow, side == YA3Side_Left ? YA3Joint_LElbow : YA3Joint_RElbow);
    projectJoint(hand, side == YA3Side_Left ? YA3Joint_LHand : YA3Joint_RHand);

    return CGLA::length(elbow - hand);
}

float InputHelper::HandToTorsoDistance(YA3Side side) const
{
    CGLA::Vec3f hand, hipcenter, shouldercenter;
    projectJoint(hand, side == YA3Side_Left ? YA3Joint_LHand : YA3Joint_RHand);
    projectJoint(hipcenter, YA3Joint_HipCenter);
    projectJoint(shouldercenter, YA3Joint_ShoulderCenter);

    return point_segment_distance(hand, hipcenter, shouldercenter);
}

float InputHelper::ElbowToTorsoDistance(YA3Side side) const
{
    CGLA::Vec3f elbow, hipcenter, shouldercenter;
    projectJoint(elbow, side == YA3Side_Left ? YA3Joint_LElbow : YA3Joint_RElbow);
    projectJoint(hipcenter, YA3Joint_HipCenter);
    projectJoint(shouldercenter, YA3Joint_ShoulderCenter);

    return point_segment_distance(elbow, hipcenter, shouldercenter);
}

float InputHelper::TorsoTiltAngle() const
{
    CGLA::Vec3f shouldercenter, hipcenter;
    projectJoint(hipcenter, YA3Joint_HipCenter);
    projectJoint(shouldercenter, YA3Joint_ShoulderCenter);

    CGLA::Vec3f torso = shouldercenter - hipcenter; //upward
    CGLA::Vec3f vertical(0, 1, 0);
    torso.normalize();

    return std::acosf(CGLA::dot(torso, vertical)) * (torso[0] < 0 ? -1 : 1); // tilting left yield negative angle
}

float InputHelper::HandDistance() const
{
    CGLA::Vec3f left, right;
    projectJoint(left, YA3Joint_LHand);
    projectJoint(right, YA3Joint_LHand);

    return CGLA::length(left - right);
}


/* Main interface for interaction */


float InputHelper::GetBranchingProbability(YA3Side side) const
{
    if(side == YA3Side_Left)
    {
        return std::max(HandToTorsoDistance(YA3Side_Left), ElbowToTorsoDistance(YA3Side_Left));
    }
    return std::max(HandToTorsoDistance(YA3Side_Right), ElbowToTorsoDistance(YA3Side_Right));
}

float InputHelper::GetBranchingAngle(YA3Side side) const
{
    return ((M_PI - HandToShoulderAngle(side))/(M_PI*2))*(M_PI*(4.0f/5.0f));
}

float InputHelper::GetTrunkBendingAngle() const
{
    return TorsoTiltAngle();
}




/* Virtual methods */

void InputHelper::Update(float timeStep)
{
    m_inputFilter->Update(timeStep);
}



/* Protected methods */

void InputHelper::UpdateJoint(YA3Joint joint, const CGLA::Vec3f& position)
{
    m_inputFilter->UpdateJoint(joint, position);
    m_rawJoints[joint] = position;
}

std::string InputHelper::GetStringRepresentation()
{
    std::stringstream result;

    for(int i = 0; i < YA3_JOINT_COUNT; ++i)
    {
        for(int j = 0; j < 3; ++j)
            result << m_joints[i][j] << ' ';
    }

    return result.str();
}
