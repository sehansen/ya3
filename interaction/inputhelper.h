#ifndef INPUTHELPER_H
#define INPUTHELPER_H

#include <vector>

#include <QObject>
#include <QTime>
#include <QTimer>

#include <CGLA/Vec3f.h>

#include "filter/inputfilter.h"
#include "skeletonutils.h"

//forward declarations
//class InputFilter;


class InputHelper : public QObject
{
    Q_OBJECT

public:
    int m_currentPlayer;
    bool m_initialized;

    InputHelper();
    virtual ~InputHelper();

    virtual bool Init() = 0; //make sure to set initialized
    bool IsInitialized() const;
    virtual void Update(float timeStep);

    const CGLA::Vec3f& JointPosition(YA3Joint joint) const;
    const CGLA::Vec3f& RawJointPosition(YA3Joint joint) const;

    float HandToShoulderAngle(YA3Side side) const;
    float HandToShoulderDistance(YA3Side side) const;
    float HandToShoulderDistance3D(YA3Side side) const;
    float ElbowToShoulderAngle(YA3Side side) const;
    float ElbowToShoulderDistance(YA3Side side) const;
    float ElbowToHandAngle(YA3Side side) const;
    float ElbowToHandDistance(YA3Side side) const;
    float HandToTorsoDistance(YA3Side side) const;
    float ElbowToTorsoDistance(YA3Side side) const;
    float TorsoTiltAngle() const;
    float HandDistance() const;

    float GetBranchingProbability(YA3Side side) const;
    float GetBranchingAngle(YA3Side side) const;
    float GetTrunkBendingAngle() const;

    std::string GetStringRepresentation();

protected:
    std::vector<CGLA::Vec3f> m_joints;
    SkeletonData m_defaultSkeleton;

    void UpdateJoint(YA3Joint joint, const CGLA::Vec3f& position);

private:
    std::vector<CGLA::Vec3f> m_rawJoints;
    InputFilter* m_inputFilter;
    void projectJoint(CGLA::Vec3f& store, YA3Joint joint) const;
};

#endif // INPUTHELPER_H
