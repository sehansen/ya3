#include "keyboardhelper.h"

#include <fstream>
#include <sstream>
#include <Windows.h>

#include <QDebug>
#include "GLGraphics/ResourceLoader.h"

#include "skeletonutils.h"


KeyboardHelper::KeyboardHelper() :
    m_target(0),
    m_time2Pi(0.0f)
{
    m_outSkeleton.resize(YA3_JOINT_COUNT);
    m_targetSkeleton.resize(YA3_JOINT_COUNT);
    m_currentPlayer = 0;
}

bool KeyboardHelper::Init()
{
    qDebug() << "[KEYBOARDHELPER] Initializing.." << endl;
    std::string textData = GLGraphics::ResourceLoader::load_text_resource("./data/poses.txt");
    std::stringstream dataStr(textData);
    while(!dataStr.eof())
    {
        std::string name;
        std::vector<CGLA::Vec3f>* joints = new std::vector<CGLA::Vec3f>();
        SkeletonUtils::LoadPose(dataStr, *joints, name);
        m_poses[name] = std::unique_ptr<std::vector<CGLA::Vec3f>>(joints);
    }


    auto const& def = *m_poses["default"].get();
    SkeletonUtils::LerpSkeleton(def, def, 1.0f, m_targetSkeleton);
    SkeletonUtils::LerpSkeleton(def, def, 1.0f, m_outSkeleton);


    qDebug() << "[KEYBOARDHELPER] Ready!" << endl;
    m_initialized = true;
    return true;
}

void KeyboardHelper::Update(float timeStep)
{
    InputHelper::Update(timeStep);

    for(int i = 0; i < 10; ++i)
    {
        if(GetAsyncKeyState('0' + i))
        {
            m_target = i;
        }
    }

    if(m_target == 0)//Restore to default
    {
        auto const& def = *m_poses["default"].get();
        //LerpSkeleton(def, def, 1.0f, m_targetSkeleton);
        //SmoothMove(m_targetSkeleton, 1.0f * timeStep, m_outSkeleton);

        SkeletonUtils::LerpSkeleton(def, def, 1.0f, m_targetSkeleton);
        SkeletonUtils::LerpSkeleton(m_outSkeleton, 2.0f*timeStep, m_targetSkeleton);
    }
    else if(m_target == 1)//Raise left hand
    {
        auto const& left = *m_poses["left_arm_135"].get();
        //LerpSkeleton(left, left, 1.0f, m_targetSkeleton);
        //SmoothMove(m_targetSkeleton, 0.2f * timeStep, m_outSkeleton);

        SkeletonUtils::LerpSkeleton(left, left, 1.0f, m_targetSkeleton);
        SkeletonUtils::LerpSkeleton(m_outSkeleton, 2.f*timeStep, m_targetSkeleton);
    }
    else if(m_target == 2)//Lower left hand
    {
        auto const& left = *m_poses["left_arm_45"].get();
        //LerpSkeleton(left, left, 1.0f, m_targetSkeleton);
        //SmoothMove(m_targetSkeleton, 0.2f * timeStep, m_outSkeleton);

        SkeletonUtils::LerpSkeleton(left, left, 1.0f, m_targetSkeleton);
        SkeletonUtils::LerpSkeleton(m_outSkeleton, 2.f*timeStep, m_targetSkeleton);
    }
    else if(m_target == 3)//Sine left arm
    {
        auto const& left45 =  *m_poses["left_arm_45"].get();
        auto const& left135 = *m_poses["left_arm_135"].get();
        //LerpSkeleton(left45, left135, 0.5f * (sinf(m_time2Pi) + 1.0f), m_outSkeleton);
        SkeletonUtils::LerpSkeleton(left45, left135, 0.5f * (sinf(m_time2Pi) + 1.0f), m_outSkeleton);
    }

    for(size_t i = 0; i < m_outSkeleton.size(); ++i)
    {
        UpdateJoint((YA3Joint)i, m_outSkeleton[i]);
    }

    m_time2Pi += timeStep * 2.4f;
    if(m_time2Pi > 2.0f * (float)M_PI)
        m_time2Pi -= 2.0f * (float)M_PI;
}

