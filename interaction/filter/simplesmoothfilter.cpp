#include "simplesmoothfilter.h"

SimpleSmoothFilter::SimpleSmoothFilter(std::vector<CGLA::Vec3f>& joints, float speed, float deadzone):
    InputFilter(joints),
    m_speed(speed),
    m_deadzone(deadzone)
{
    m_target = joints;
}

void SimpleSmoothFilter::UpdateJoint(YA3Joint joint, const CGLA::Vec3f& point)
{
    m_target[joint] = point;
}

void SimpleSmoothFilter::Update(float timeStep)
{
    for(int i = 0; i < YA3_JOINT_COUNT; ++i)
    {
        CGLA::Vec3f vec = m_speed * timeStep * (m_target[i] - m_resultJoints[i]);
        float len = vec.length();
        vec /= len;

        len -= m_deadzone;
        if(len >= 0.0f)
        {
            m_resultJoints[i] += vec * len;
        }
    }
}
