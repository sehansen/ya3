#include "nofilter.h"

NoFilter::NoFilter(std::vector<CGLA::Vec3f> &joints):
    InputFilter(joints)
{
}

void NoFilter::UpdateJoint(YA3Joint joint, const CGLA::Vec3f& point)
{
    m_resultJoints[joint] = point;
}

void NoFilter::Update(float)
{

}
