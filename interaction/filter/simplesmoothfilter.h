#ifndef SIMPLESMOOTHFILTER_H
#define SIMPLESMOOTHFILTER_H

#include "inputfilter.h"

#include <vector>

#include <CGLA/Vec3f.h>



class SimpleSmoothFilter : public InputFilter
{
public:
    SimpleSmoothFilter(std::vector<CGLA::Vec3f> &joints, float speed, float deadzone);

    virtual void Update(float timeStep);
    virtual void UpdateJoint(YA3Joint joint, const CGLA::Vec3f& point);


    float GetSpeed() const { return m_speed; }
    void SetSpeed(float speed) { m_speed = speed; }
private:

    std::vector<CGLA::Vec3f> m_target;
    float m_speed;
    float m_deadzone;
};

#endif // SIMPLESMOOTHFILTER_H
