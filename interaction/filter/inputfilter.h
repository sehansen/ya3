#ifndef INPUTFILTER_H
#define INPUTFILTER_H

#include <CGLA/Vec3f.h>

#include "../skeletonutils.h"

class InputFilter
{
public:
    InputFilter(std::vector<CGLA::Vec3f>& joints);

    virtual void Update(float timeStep) = 0;
    virtual void UpdateJoint(YA3Joint joint, const CGLA::Vec3f& point) = 0;

protected:
    std::vector<CGLA::Vec3f>& m_resultJoints;
};

#endif // INPUTFILTER_H
