#ifndef NOFILTER_H
#define NOFILTER_H

#include "inputfilter.h"

class NoFilter : public InputFilter
{
public:
    NoFilter(std::vector<CGLA::Vec3f>& joints);

    virtual void Update(float timeStep);
    virtual void UpdateJoint(YA3Joint joint, const CGLA::Vec3f& point);
};

#endif // NOFILTER_H
