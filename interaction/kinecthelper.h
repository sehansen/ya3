#ifndef KINECTHELPER_H
#define KINECTHELPER_H

#include "inputhelper.h"
#include <Windows.h>
#include <Ole2.h>
#include <NuiApi.h>
#include <unordered_map>

class KinectHelper : public InputHelper
{
public:
    KinectHelper();
    virtual ~KinectHelper();

    virtual bool Init();
    virtual void Update(float timeStep);

    void FindCurrentPlayer(NUI_SKELETON_FRAME& sframe);

private:
    QTimer* FindCurrentPlayerTimer;
    int m_findCurrentPlayerInterval;

    SkeletonData m_defaultPoseIntermediate;

    INuiSensor* m_sensor;
    HANDLE m_skeletonEventListener;
};

#endif // KINECTHELPER_H
