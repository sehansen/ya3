#ifndef SKELETONUTILS_H
#define SKELETONUTILS_H

#include <vector>
#include <sstream>

#include <CGLA/Vec3f.h>

typedef std::vector<CGLA::Vec3f> SkeletonData;

enum YA3Side
{
    YA3Side_Left,
    YA3Side_Right,
    YA3Side_NoSide
};

enum YA3Joint
{
    YA3Joint_LHand,
    YA3Joint_LElbow,
    YA3Joint_LShoulder,
    YA3Joint_ShoulderCenter,
    YA3Joint_HipCenter,
    YA3Joint_RHand,
    YA3Joint_RElbow,
    YA3Joint_RShoulder,
    YA3_JOINT_COUNT
};

class SkeletonUtils
{
public:
    static void LoadPose(std::stringstream &istream, SkeletonData &joints, std::string& name);
    static void LerpSkeleton(const SkeletonData& a, const SkeletonData& b, float t, SkeletonData& out);
    static void LerpSkeleton(std::vector<CGLA::Vec3f>& source, float factor, const std::vector<CGLA::Vec3f>& target);

private:
    SkeletonUtils();
};

#endif // SKELETONUTILS_H
