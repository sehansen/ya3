#-------------------------------------------------
#
# Project created by QtCreator 2014-09-11T08:28:15
#
#-------------------------------------------------

QT += core gui opengl multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = YA3

TEMPLATE = app


# Include Paths
INCLUDEPATH += "$${PWD}/libs/"
win32 {
    INCLUDEPATH += $${PWD}"/libs/glew-1.10.0/include/"
    INCLUDEPATH += $$(KINECTSDK10_DIR)"/inc"

    !contains(QMAKE_TARGET.arch, x86_64) {
        message("x86 build")
        LIBS *=  "$${PWD}/libs/glew-1.10.0/lib/Release/Win32/glew32s.lib"
        LIBS *= $$(KINECTSDK10_DIR)"/lib/x86/Kinect10.lib"

    }
    else {
        message("x86_64 build")
        LIBS *= "$${PWD}/libs/glew-1.10.0/lib/Release/x64/glew32s.lib"
        LIBS *= "$$(KINECTSDK10_DIR)/lib/amd64/Kinect10.lib"
    }
}


HEADERS  += \
    controlwindow.h \
    libs/CGLA/ArithMatFloat.h \
    libs/CGLA/ArithQuat.h \
    libs/CGLA/ArithSqMat2x2Float.h \
    libs/CGLA/ArithSqMat3x3Float.h \
    libs/CGLA/ArithSqMat4x4Float.h \
    libs/CGLA/ArithSqMatFloat.h \
    libs/CGLA/ArithVec.h \
    libs/CGLA/ArithVec2Float.h \
    libs/CGLA/ArithVec3Float.h \
    libs/CGLA/ArithVec3Int.h \
    libs/CGLA/ArithVec4Float.h \
    libs/CGLA/ArithVec4Int.h \
    libs/CGLA/ArithVecFloat.h \
    libs/CGLA/ArithVecInt.h \
    libs/CGLA/BitMask.h \
    libs/CGLA/CGLA.h \
    libs/CGLA/eigensolution.h \
    libs/CGLA/ExceptionStandard.h \
    libs/CGLA/Mat2x2d.h \
    libs/CGLA/Mat2x2f.h \
    libs/CGLA/Mat2x3d.h \
    libs/CGLA/Mat2x3f.h \
    libs/CGLA/Mat3x3d.h \
    libs/CGLA/Mat3x3f.h \
    libs/CGLA/Mat4x4d.h \
    libs/CGLA/Mat4x4f.h \
    libs/CGLA/Quatd.h \
    libs/CGLA/Quaternion.h \
    libs/CGLA/Quatf.h \
    libs/CGLA/statistics.h \
    libs/CGLA/TableTrigonometry.h \
    libs/CGLA/UnitVector.h \
    libs/CGLA/Vec2d.h \
    libs/CGLA/Vec2f.h \
    libs/CGLA/Vec2i.h \
    libs/CGLA/Vec2ui.h \
    libs/CGLA/Vec3d.h \
    libs/CGLA/Vec3f.h \
    libs/CGLA/Vec3Hf.h \
    libs/CGLA/Vec3i.h \
    libs/CGLA/Vec3uc.h \
    libs/CGLA/Vec3usi.h \
    libs/CGLA/Vec4d.h \
    libs/CGLA/Vec4f.h \
    libs/CGLA/Vec4i.h \
    libs/CGLA/Vec4uc.h \
    libs/GLGraphics/Core3_2_context.h \
    libs/GLGraphics/GLHeader.h \
    libs/GLGraphics/ResourceLoader.h \
    libs/GLGraphics/ShaderProgram.h \
    libs/GLGraphics/ThreeDObject.h \
    libs/GLGraphics/User.h \
    libs/Mesh/Material.h \
    libs/Mesh/ObjLoader.h \
    libs/Mesh/Texmap.h \
    libs/Mesh/TriangleMesh.h \
    libs/glew-1.10.0/include/GL/glew.h \
    libs/glew-1.10.0/include/GL/glxew.h \
    libs/glew-1.10.0/include/GL/wglew.h \
    interaction/kinecthelper.h \
    interaction/inputhelper.h \
    interaction/keyboardhelper.h \
    interaction/filter/inputfilter.h \
    interaction/filter/simplesmoothfilter.h \
    interaction/filter/nofilter.h \
    interaction/skeletonutils.h \
    scene/Scene.h \
    scene/Camera.h \
    scene/FloatingCamera.h \
    scene/MovableCamera.h \
    scene/SceneObject.h \
    generation/turtle.h \
    generation/TreeData.h \
    rendering/Renderer.h \
    rendering/ShaderCache.h \
    rendering/GBuffer.h \
    rendering/ToonRenderer.h \
    flags.h \
    soundsystem.h \
    scene/objects/Tree.h \
    scene/objects/Scene3DObject.h \
    scene/objects/SceneMeshObject.h \
    scene/objects/Terrain.h \
    scene/objects/Skybox.h \
    auxil.h \
    debugwindow.h \
    rendering/ShadowBuffer.h

SOURCES += \
    main.cpp \
    controlwindow.cpp \
    libs/CGLA/ArithSqMat3x3Float.cpp \
    libs/CGLA/ArithSqMat4x4Float.cpp \
    libs/CGLA/ArithVec2Float.cpp \
    libs/CGLA/ArithVec3Float.cpp \
    libs/CGLA/eigensolution.cpp \
    libs/CGLA/gel_rand.cpp \
    libs/CGLA/Mat3x3d.cpp \
    libs/CGLA/Mat3x3f.cpp \
    libs/CGLA/Mat4x4d.cpp \
    libs/CGLA/Mat4x4f.cpp \
    libs/CGLA/statistics.cpp \
    libs/CGLA/TableTrigonometry.cpp \
    libs/CGLA/Vec2i.cpp \
    libs/CGLA/Vec3f.cpp \
    libs/CGLA/Vec3i.cpp \
    libs/GLGraphics/GLHeader.cpp \
    libs/GLGraphics/ResourceLoader.cpp \
    libs/GLGraphics/ShaderProgram.cpp \
    libs/GLGraphics/ThreeDObject.cpp \
    libs/GLGraphics/User.cpp \
    libs/Mesh/ObjLoader.cpp \
    libs/Mesh/Texmap.cpp \
    libs/Mesh/TriangleMesh.cpp \
    generation/turtle.cpp \
    generation/TreeData.cpp\
    interaction/kinecthelper.cpp \
    interaction/inputhelper.cpp \
    interaction/keyboardhelper.cpp \
    interaction/filter/inputfilter.cpp \
    interaction/filter/simplesmoothfilter.cpp \
    interaction/filter/nofilter.cpp \
    interaction/skeletonutils.cpp \
    scene/Scene.cpp \
    scene/Camera.cpp \
    scene/MovableCamera.cpp\
    scene/FloatingCamera.cpp \
    rendering/ShaderCache.cpp \
    rendering/GBuffer.cpp \
    rendering/Renderer.cpp \
    rendering/ToonRenderer.cpp \
    scene/objects/Scene3DObject.cpp \
    scene/objects/Tree.cpp \
    scene/objects/Terrain.cpp \
    auxil.cpp \
    soundsystem.cpp \
    debugwindow.cpp \
    rendering/ShadowBuffer.cpp

FORMS += controlwindow.ui \
    debugwindow.ui

OTHER_FILES += \
    shaders/TerrainScene/wire.geom \
    shaders/TerrainScene/fur.geom \
    shaders/TerrainScene/deferred_ssao.frag \
    shaders/TerrainScene/deferred_ssao_combination.frag \
    shaders/TerrainScene/deferred_toon.frag \
    shaders/TerrainScene/fur.frag \
    shaders/TerrainScene/instanced_object.frag \
    shaders/TerrainScene/instanced_objects_gbuffer.frag \
    shaders/TerrainScene/object.frag \
    shaders/TerrainScene/objects_gbuffer.frag \
    shaders/TerrainScene/shadow.frag \
    shaders/TerrainScene/terrain.frag \
    shaders/TerrainScene/terrain_gbuffer.frag \
    shaders/TerrainScene/tree.frag \
    shaders/TerrainScene/tree_gbuffer.frag \
    shaders/TerrainScene/wire.frag \
    shaders/TerrainScene/deferred.vert \
    shaders/TerrainScene/fur.vert \
    shaders/TerrainScene/instanced_object.vert \
    shaders/TerrainScene/instanced_objects_gbuffer.vert \
    shaders/TerrainScene/object.vert \
    shaders/TerrainScene/objects_gbuffer.vert \
    shaders/TerrainScene/shadow.vert \
    shaders/TerrainScene/shadow_trees.vert \
    shaders/TerrainScene/terrain.vert \
    shaders/TerrainScene/terrain_gbuffer.vert \
    shaders/TerrainScene/tree.vert \
    shaders/TerrainScene/tree_gbuffer.vert \
    shaders/TerrainScene/wire.vert \
    install.txt \
    TODO.txt \
    herebedragons.txt
